# Shakken - a simple chess engine#

Shakken is a simple chess engine written in C++ with an alpha-beta algorithm and custom evaluation function.

![shakken.jpg](https://bitbucket.org/repo/ypKbe76/images/3174704826-shakken.jpg)

# Features: #
* Custom console 
* Graphical representation of board using SFML
* Debug overlays (in Finnish) with dear imgui integration
* Multi-threading
* Alpha-beta algorithm based AI
* All legal chess moves

Note: all text and some of the code is in Finnish

### Build requirements ###

All dependencies should be included, including SFML 2.4.4

Note: Shakken needs to be compiled in 32-bit
 

### Credits ###

* Alex Hartto - programming
* Daniel Laaksonen - programming
* [https://www.sfml-dev.org/](https://www.sfml-dev.org/) - basic GUI
* [dear imgui](https://github.com/ocornut/imgui) - for debug overlays
* [imgui-sfml](https://github.com/eliasdaler/imgui-sfml) - SFML backend for dear imgui