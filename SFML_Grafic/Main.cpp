#include <SFML\Graphics.hpp>
#include <iostream>
#include <list>
#include "Shakki\Asema.h"
#include "Shakki\Kayttoliittyma.h"
#include "debuggeri.h"

#define CELL_SZ 60


Asema asema;
Kayttoliittyma* kayttis = Kayttoliittyma::getInstance();
sf::Font chess_font;
sf::RenderWindow window(sf::VideoMode(800, 800), "Shakken");

static const ImWchar chess_font_range[] = { 0x2654, 0x265F, 0 };

int draw_board(std::vector<sf::RectangleShape>& board);
int draw_board_pieces(const Asema& asema, std::vector<sf::Text>& output_pieces, const sf::Font& font);
int draw_board_letters(std::vector<sf::Text>& output_letters, const sf::Font& font);
//void debug_piece_moves(Asema& asema);
Debuggeri debuggeri(&asema);

Ruutu promotionRuutu;


int main()
{
	asema.luoLauta();
	kayttis->asetaAsema(&asema);

	//setup dear imgui


	window.resetGLStates();
	// Merge chess pieces to default font
	ImGuiIO& io = ImGui::GetIO();
	ImFontConfig cfg;
	io.Fonts->AddFontDefault(&cfg);
	cfg.MergeMode = true;
	io.Fonts->AddFontFromFileTTF("DejaVuSansMono.ttf", 24.0f, &cfg, chess_font_range);
	unsigned char* p;
	int w;
	int h;
	int bytes;
	io.Fonts->GetTexDataAsRGBA32(&p, &w, &h, &bytes);
	ImGui::SFML::Init(window);

	sf::Text testi;
	chess_font.loadFromFile("DejaVuSansMono.ttf");
	testi.setFont(chess_font);
	testi.setString(L"\u2654");
	testi.setPosition(20, 25);


	// these are static assets that will not change
	std::vector<sf::RectangleShape> chess_board;
	draw_board(chess_board);
	std::vector<sf::Text> board_letters;
	draw_board_letters(board_letters, chess_font);

	// chess pieces are made each time they change position
	std::vector<sf::Text> chess_pieces;
	sf::Text input;
	input.setFont(chess_font);

	input.setString("1 Valkoinen 2 Musta");
	input.setPosition(sf::Vector2f(0, 9 * CELL_SZ));


	sf::Clock clock;
	window.setFramerateLimit(60);


	bool show_debug = false;
	bool pause = false;
	while (window.isOpen())
	{

		bool render_pieces = true;
		bool render = true;
		sf::Event event;


		while (window.pollEvent(event))
		{
			
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			else if (event.type == sf::Event::TextEntered) //&& show_debug == false)
			{
				if (!kayttis->onkoKorotus())
				{
					if (input.getString() == "Anna siirto muodossa g1-f3")
						input.setString("");
					if (event.text.unicode < 128 &&
						event.text.unicode >= 32)
						input.setString(input.getString() + event.text.unicode);
				}
			}
			if (event.type == sf::Event::KeyReleased)
			{
				if (kayttis->onkoKorotus())
				{
					if (kayttis->korotaNappula(event.key.code))
					{
						kayttis->vaihdaVuoro();
						input.setString("Anna siirto muodossa g1-f3");
					}
				}
				else if (kayttis->onkoAloitus())
				{
					if(!kayttis->vain_ai)
					{
						if (event.key.code == sf::Keyboard::Num1)
						{
							input.setString("Anna siirto muodossa g1-f3");
							kayttis->valitsePuoli(Asema::VALK);
						}
						else if (event.key.code == sf::Keyboard::Num2)
						{
							input.setString("Anna siirto muodossa g1-f3");
							kayttis->valitsePuoli(Asema::MUST);
						}
					}
					else
					{
						kayttis->kaynnistaAI();
					}

				}
				if (event.key.code == sf::Keyboard::F1)
				{
					if (show_debug == false)
					{
						show_debug = true;
					}
					else if (show_debug == true)
					{
						show_debug = false;
					}
				}

				if (event.key.code == sf::Keyboard::P)
				{
					if (pause == false)
					{
						pause = true;
					}
					else if (pause == true)
					{
						pause = false;
					}
				}
			}
			else if (event.key.code == sf::Keyboard::Return)// && show_debug == false)
			{

				if (!kayttis->onkoKorotus())
				{
					if (input.getString().getSize() > 0)
					{
						kayttis->lueSyote(input.getString());
						if (kayttis->onkoKorotus())
						{
							input.setString("Korotus: (T) Torni, (R) Ratsu,\n (K) Kuningatar, (L) L�hetti");
						}
						else
						{
							input.setString("Anna siirto muodossa g1-f3");
						}
						render_pieces = true;
					}
				}


			}
			else if (event.key.code == sf::Keyboard::BackSpace) //&& show_debug == false)
			{
				if (!kayttis->onkoKorotus())
				{
					sf::String str = input.getString();
					if (str.getSize() > 0)
						str.erase(str.getSize() - 1, 1);
					input.setString(str);
				}
			}

			render = true;
		}

		if (render)
		{
			window.clear();
			if (render_pieces)
			{
				// update piece position
				draw_board_pieces(asema, chess_pieces, chess_font);
				render_pieces = false;
			}

			for (auto&& b : chess_board)
			{
				window.draw(b);
			}
			for (auto&& letter : board_letters)
			{
				window.draw(letter);
			}
			for (auto&& piece : chess_pieces)
			{
				window.draw(piece);
			}
			window.draw(input);
			// DEBUG code
			if (show_debug)
			{
				ImGui::SFML::ProcessEvent(event);
				ImGui::SFML::Update(window, sf::Time(sf::seconds(1.0 / 60.0)));
				debuggeri.debug_piece_moves();
				debuggeri.draw_debug(window);
				ImGui::Render();
			}
			window.display();
			render = false;
		}

		if (asema.shakkimatti == true)
		{
			input.setString("Shakkimatti.");
		}
		else if (!pause && kayttis->onkoAIVuoro() && kayttis->peliOhi == false)
		{
			sf::sleep(sf::seconds(1));
			std::cout << "\n\nVuoro!!! : " << asema.getSiirtovuoro();
			kayttis->tekoalySiirto();
		}
	
	}
	ImGui::SFML::Shutdown();
	return 0;
}

int draw_board_pieces(const Asema& asema, std::vector<sf::Text>& output_pieces, const sf::Font& font)
{
	output_pieces.clear();
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (asema.lauta[i][j] != asema.tyhja)
			{
				sf::Text piece;
				piece.setFont(font);
				piece.setCharacterSize(CELL_SZ);
				int centering = CELL_SZ / 5;

				// SFML draws from upper left, but we want to draw pieces the other way aroudn
				piece.setPosition(sf::Vector2f(CELL_SZ*j + centering, 7 * CELL_SZ - CELL_SZ*i - centering));

				if (asema.lauta[i][j]->Vari() == Asema::VALK)
				{
					// change the symbol to black piece 
					// black pieces are uniform instead of transparent
					std::wstring white_to_blackpiece = asema.lauta[i][j]->Unicode();
					white_to_blackpiece[0] += 6; // 6 gives the black piece in unicode

					piece.setString(white_to_blackpiece);
					piece.setFillColor(sf::Color(255, 255, 255));
					piece.setOutlineColor(sf::Color::Black);
					piece.setOutlineThickness(1);
				}
				else if (asema.lauta[i][j]->Vari() == Asema::MUST)
				{
					piece.setString(asema.lauta[i][j]->Unicode());
					piece.setFillColor(sf::Color::Black);
					piece.setOutlineColor(sf::Color::White);
					piece.setOutlineThickness(1);
				}
				output_pieces.push_back(piece);
			}
		}
	}
	return 0;
}

int draw_board_letters(std::vector<sf::Text>& letters, const sf::Font& font)
{
	for (int i = 0; i < 8; i++)
	{
		char col = i + 'A';
		char row = '8' - i;
		sf::Text colLetter;
		sf::Text rowLetter;

		colLetter.setFont(font);
		colLetter.setString(col);
		colLetter.setPosition(i*CELL_SZ + CELL_SZ / 5, 8 * CELL_SZ);

		rowLetter.setFont(font);
		rowLetter.setString(row);
		rowLetter.setPosition(8 * CELL_SZ, i * CELL_SZ + CELL_SZ / 5);

		letters.push_back(colLetter);
		letters.push_back(rowLetter);
	}
	return 0;
}


int draw_board(std::vector<sf::RectangleShape>& board)
{
	int x = 0;
	int y = 0;
	int w = CELL_SZ;
	int h = CELL_SZ;
	int count = 1;
	board.resize(64);
	for (int i = 0; i < 8; i++)
	{
		for (int k = 0; k < 8; k++)
		{
			x = w*i;
			y = h*k;

			if (count % 2 == 1)
			{
				board[i * 8 + k] = sf::RectangleShape(sf::Vector2f(CELL_SZ, CELL_SZ));
				board[i * 8 + k].setFillColor(sf::Color(128, 128, 54));
				board[i * 8 + k].setPosition(sf::Vector2f(x, y));
			}
			else
			{
				board[i * 8 + k] = sf::RectangleShape(sf::Vector2f(CELL_SZ, CELL_SZ));
				board[i * 8 + k].setFillColor(sf::Color::Black);
				board[i * 8 + k].setPosition(sf::Vector2f(x, y));
			}
			count++;
		}
		count++;
	}
	return 0;
}
