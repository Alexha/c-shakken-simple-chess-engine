#include <iostream>
#include <chrono>
class Ajastin {
public:
	Ajastin(std::string msg) : function(msg) { start = std::chrono::high_resolution_clock::now(); }
	~Ajastin() { 
		end = std::chrono::high_resolution_clock::now();
		std::chrono::nanoseconds duration =  end - start;
		std::chrono::duration<double, std::milli> durinms = duration;
		std::cout << "\n" << function.c_str() << " vei " << durinms.count() << "ms"
			<< "tai " << duration.count() << "ns";
	}
	
private:
	std::string function;
	std::chrono::time_point<std::chrono::steady_clock> start;
	std::chrono::time_point<std::chrono::steady_clock> end;
	
};