#pragma once

#include "..\Shakki\Asema.h"

const double  VALK_HAVIO = std::numeric_limits<double>::lowest();
const double  MUST_HAVIO = std::numeric_limits<double>::max();

const int QDEPTH = 2;

class Asema;
struct siirtoArvo
{
public:
	double arvo;
	Siirto siirto;
};


class Minimax
{
public:

	double valkoinen(const int depth, Asema asema, double alpha, double beta);
	double musta(const int depth, Asema asema, double alpha, double beta);

	void jarjestaSiirrot(std::vector<Siirto>& siirrot, Asema asema, int vari, double arvo, double alpha, double beta);
	siirtoArvo arvaus(const int depth, Asema a, double alpha, double beta);

	siirtoArvo parallel_arvaus(const int depth, Asema asema, double alpha, double beta);
	double Qhaku(int depth, Asema & asema, double alpha, double beta);

	double syontiSarjaEvaluointi(Siirto siirto, Asema asema, int vari);



	double laskeMateriaali(Asema & asema);
	double laskePositio(Asema & asema);

	double estetytNappulat(Asema & asema);

	double evaluate(Asema& asema);

private:
	Siirto parasSiirto;
	Siirto parasArvaus;

	const short sotilaat[64] =
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		-6,  -4,   1,   1,   1,   1,  -4,  -6,
		-6,  -4,   1,   2,   2,   1,  -4,  -6,
		-6,  -4,   2,   8,   8,   2,  -4,  -6,
		-6,  -4,   5,  10,  10,   5,  -4,  -6,
		-4,  -4,   1,   5,   5,   1,  -4,  -4,
		-6,  -4,   1, -24,  -24,  1,  -4,  -6,
		0,   0,   0,   0,   0,   0,   0,   0
	};

	const short ratsut[64] =
	{
		-8,  -8,  -8,  -8,  -8,  -8,  -8,  -8,
		-8,   0,   0,   0,   0,   0,   0,  -8,
		-8,   0,   4,   6,   6,   4,   0,  -8,
		-8,   0,   6,   8,   8,   6,   0,  -8,
		-8,   0,   6,   8,   8,   6,   0,  -8,
		-8,   0,   4,   6,   6,   4,   0,  -8,
		-8,   0,   1,   2,   2,   1,   0,  -8,
		-16, -12,  -8,  -8,  -8,  -8, -12,  -16
	};


	const short lahetit[64] =
	{
		-4,  -4,  -4,  -4,  -4,  -4,  -4,  -4,
		-4,   0,   0,   0,   0,   0,   0,  -4,
		-4,   0,   2,   4,   4,   2,   0,  -4,
		-4,   0,   4,   6,   6,   4,   0,  -4,
		-4,   0,   4,   6,   6,   4,   0,  -4,
		-4,   1,   2,   4,   4,   2,   1,  -4,
		-4,   2,   1,   1,   1,   1,   2,  -4,
		-4,  -4, -12,  -4,  -4, -12,  -4,  -4
	};

	const short kuningas[64] =
	{
		-40, -30, -50, -70, -70, -50, -30, -40,
		-30, -20, -40, -60, -60, -40, -20, -30,
		-20, -10, -30, -50, -50, -30, -10, -20,
		-10,   0, -20, -40, -40, -20,   0, -10,
		0,  10, -10, -30, -30, -10,  10,   0,
		10,  20,   0, -20, -20,   0,  20,  10,
		30,  40,  20,   0,   0,  20,  40,  30,
		40,  50,  30,  10,  10,  30,  50,  40
	};
	const short torni[64] =
	{

		5,   5,   5,   5,   5,   5,   5,   5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		-5,   0,   0,   0,   0,   0,   0,  -5,
		0,   0,   0,   2,   2,   0,   0,   0
	};

	const short kuningatar[64] =
	{
		0,   0,   0,   0,   0,   0,   0,   0,
		0,   0,   1,   1,   1,   1,   0,   0,
		0,   0,   1,   2,   2,   1,   0,   0,
		0,   0,   2,   3,   3,   2,   0,   0,
		0,   0,   2,   3,   3,   2,   0,   0,
		0,   0,   1,   2,   2,   1,   0,   0,
		0,   0,   1,   1,   1,   1,   0,   0,
		-5,  -5,  -5,  -5,  -5,  -5,  -5,  -5
	};


	const int DAAMI_ARVO = 975;
	const int TORNI_ARVO = 500;
	const int LAHETTI_ARVO = 335;
	const int RATSU_ARVO = 325;
	const int SOTILAS_ARVO = 100;
	const int KUNINGAS_ARVO = 1000;

	const int TORNI_PARI = 16;
	const int RATSU_PARI = 8;
	const int LAHETTI_PARI = 30;

	const int KUNINGAS_ESTA_TORNIA= 24;
	const int KESKINAPPULA_ESTA = 24;
	const int LAHETTI_A7_ANSA = 150;
	const int LAHETTI_A6_ANSA = 50;
	const int RATSU_A8_ANSA = 150;
	const int RATSU_A7_ANSA = 100;
};
