#include "Kuningas.h"
#include "Asema.h"
#include "Siirto.h"


static const int kuningasSiirrot[8][2] = {
	{ 1,0 },
	{ 1,1 },
	{ 1,-1 },
	{ 0,1 },
	{ 0,-1 },
	{ -1,-1 },
	{ -1,1 },
	{ -1,0 },
	
};

void Kuningas::annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulaSijainti, Asema* asema, int vari) const
{
	int alkuRivi = nappulaSijainti->rivi;
	int alkuSarake = nappulaSijainti->sar;

	for (int i = 0; i < 8; i++)
	{
		int i_rivi = alkuRivi + kuningasSiirrot[i][0];
		int i_sar = alkuSarake + kuningasSiirrot[i][1];
		if (i_rivi >= 0 && i_sar >= 0 &&
			i_rivi < 8 && i_sar < 8)
		{
			if (asema->lauta[i_rivi][i_sar] == asema->tyhja || asema->lauta[i_rivi][i_sar]->Vari() != vari)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i_rivi, i_sar)));
			}
		}

	}


}
