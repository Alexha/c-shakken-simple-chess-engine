#pragma once
#include <iostream>
#include "Asema.h"
#include "Siirto.h"
#include "..\Minimax.h"

class Kayttoliittyma
{
public:
	static Kayttoliittyma* getInstance();
	void asetaAsema(Asema* asema) { this->asema = asema; }
	void vaihdaVuoro();


	bool lueSyote(std::string syote);


	bool korotaNappula(int nappula);
	
	bool onkoKorotus() { return state == KOROTUS; };
	bool onkoAloitus() { return state == ALOITUS; };
	bool onkoAIVuoro() { return state == AI_VUORO; };

	void tekoalySiirto();
	void valitsePuoli(int vari) { 
		pelaajaPuoli = vari; 
		if (vari == 0) 
			state = NORMAALI; 
		else 
			state = AI_VUORO; }
	bool vain_ai = false;
	bool peliOhi = false;
	void kaynnistaAI() { state = AI_VUORO; }
private:
	Minimax tekoaly;
	Asema* asema;
	Ruutu promotionRuutu;
	int pelaajaPuoli = 0;
	enum State { NORMAALI, KOROTUS, ALOITUS, AI_VUORO };

	State state = ALOITUS;
	Siirto annaVastustajanSiirto(std::string syote);
	Kayttoliittyma(Asema* asema) : asema(asema) {};
	Kayttoliittyma() {};
	static Kayttoliittyma* instance; // osoitin luokan ainoaa instanssiin
};

