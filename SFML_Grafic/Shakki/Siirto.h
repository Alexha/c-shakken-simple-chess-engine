#pragma once
#include "Ruutu.h"

class Siirto {
public:
	Siirto(Ruutu alku, Ruutu loppu, bool syova): alkuRuutu(alku), loppuRuutu(loppu), syo(syova) {};
	Siirto(Ruutu alku, Ruutu loppu) : alkuRuutu(alku), loppuRuutu(loppu), syo(false) {};
//	Siirto(const Siirto& siirto) { alkuRuutu = siirto.alkuRuutu; loppuRuutu = siirto.loppuRuutu; };
	Siirto() = default;
	Siirto(bool lyhytLinna, bool pitkaLinna) : lyhytLinna(lyhytLinna), pitkaLinna(pitkaLinna) 
	{
	
	}; // Poikkeussiirto linnoitusta varten
	Ruutu& getAlkuruutu() { return alkuRuutu; };
	Ruutu& getLoppuruutu() { return loppuRuutu; };
	bool onkoLyhytLinna() { return lyhytLinna; };
	bool onkoPitkalinna() { return pitkaLinna; };
	bool operator==(Siirto rhs)
	{
		return (rhs.alkuRuutu == alkuRuutu && rhs.loppuRuutu == loppuRuutu);
	}
	bool syo;
private:

	Ruutu alkuRuutu;
	Ruutu loppuRuutu;
	int miksiKorotetaan = 0;
	bool lyhytLinna = false;
	bool pitkaLinna = false;
};
