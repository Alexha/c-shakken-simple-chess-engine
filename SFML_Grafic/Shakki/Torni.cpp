#include "Torni.h"
#include "Asema.h"
#include "Siirto.h"

void Torni::annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulaSijainti, Asema* asema, int vari) const
{
	int alkuRivi = nappulaSijainti->rivi;
	int alkuSarake = nappulaSijainti->sar;

	for (int i = alkuRivi+1; i < 8; i++)
	{
		if (asema->lauta[i][alkuSarake] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake)));
		}
		else if (asema->lauta[i][alkuSarake]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake), true));
			break;
		}
		else
			break;
	}

	for (int i = alkuRivi-1; i >= 0; i--)
	{
		if (asema->lauta[i][alkuSarake] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake)));
		}
		else if (asema->lauta[i][alkuSarake]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake), true));
			break;
		}
		else
			break;
	}

	for (int i = alkuSarake+1; i < 8; i++)
	{
		if (asema->lauta[alkuRivi][i] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(alkuRivi, i)));
		}
		else if (asema->lauta[alkuRivi][i]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(alkuRivi, i), true));
			break;
		}
		else
			break;
	}

	for (int i = alkuSarake-1; i >= 0; i--)
	{
		if (asema->lauta[alkuRivi][i] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(alkuRivi, i)));
		}
		else if (asema->lauta[alkuRivi][i]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(alkuRivi, i), true));
			break;
		}
		else
			break;
	}
}
