#include "Sotilas.h"
#include "Asema.h"
#include "Siirto.h"
#include <vector>


void Sotilas::annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulaSijainti, Asema* asema, int vari) const
{

	int alkuRivi = nappulaSijainti->rivi;
	int alkuSarake = nappulaSijainti->sar;

	if (vari == VALK) {
			if (asema->lauta[3][alkuSarake] == asema->tyhja && alkuRivi == 1&& asema->lauta[2][alkuSarake] == asema->tyhja) {
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(3, alkuSarake)));
			}
		int i = alkuRivi + 1;
		if (i < 8)
		{
			if (asema->lauta[i][alkuSarake] == asema->tyhja)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake)));
			}
			if (alkuSarake != 7 && asema->lauta[i][alkuSarake + 1] != asema->tyhja && asema->lauta[i][alkuSarake + 1]->Vari() != vari)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake + 1), true));
			}
			if (alkuSarake != 0 && asema->lauta[i][alkuSarake - 1] != asema->tyhja
				&& asema->lauta[i][alkuSarake - 1]->Vari() != vari)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake - 1), true));
			}
		}
	}
	else {

			if (asema->lauta[4][alkuSarake] == asema->tyhja && alkuRivi == 6 && asema->lauta[5][alkuSarake] == asema->tyhja) {
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(4, alkuSarake)));
			}


		int i = alkuRivi - 1;
		if (i > 0)
		{
			if (asema->lauta[i][alkuSarake] == asema->tyhja)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake)));
			}
			if (alkuSarake != 7 && asema->lauta[i][alkuSarake + 1] != asema->tyhja && asema->lauta[i][alkuSarake + 1]->Vari() != vari)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake + 1), true));
			}
			if (alkuSarake != 0 && asema->lauta[i][alkuSarake - 1] != asema->tyhja && asema->lauta[i][alkuSarake - 1]->Vari() != vari)
			{
				lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(i, alkuSarake - 1), true));
			}
		}
	}
}