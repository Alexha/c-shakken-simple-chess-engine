#include "Ratsu.h"
#include "Asema.h"

void Ratsu::annaSiirrot(std::vector<Siirto>& lista, Ruutu * nappulanSijainti, Asema *asema, int vari) const
{
	int rivi = nappulanSijainti->rivi;
	int sar = nappulanSijainti->sar;
	
	for (int i = 0; i < 8; i++)
	{
		int i_rivi = rivi + ratsuSiirrot[i][0];
		int i_sar = sar + ratsuSiirrot[i][1];
		if (i_rivi >= 0 && i_sar >= 0 && 
			i_rivi < 8 && i_sar < 8)
		{
			if (asema->lauta[i_rivi][i_sar] == asema->tyhja)
				lista.emplace_back(Siirto(Ruutu(rivi, sar), Ruutu(i_rivi, i_sar)));
			else if (asema->lauta[i_rivi][i_sar]->Vari() != vari)
				lista.emplace_back(Siirto(Ruutu(rivi, sar), Ruutu(i_rivi, i_sar),true));
		}

	}


};
