#pragma once
#include "Siirto.h"
#include "Torni.h"
#include "Lahetti.h"
#include "Kuningas.h"
#include "Kuningatar.h"
#include "Sotilas.h"
#include "Ratsu.h"
class Asema {

public:
	enum vuoro {
		VALK,
		MUST
	};

	enum sarake {
		a,b,c,d,e,f,g,h
	};

	//Konstruktori luo alkuaseman laudalle
	const Nappula* lauta[8][8];
	 

	void luoLauta();

	void paivitaAsema(Siirto* siirto);
	void peruutaSiirto()
	{
		lauta[viimeinenSiirto.getLoppuruutu().rivi][viimeinenSiirto.getLoppuruutu().sar] = loppuNappula;
		lauta[viimeinenSiirto.getAlkuruutu().rivi][viimeinenSiirto.getAlkuruutu().rivi] = alkuNappula;
	}
	void annaLaillisetSiirrot(std::vector<Siirto>& lista);
	void poistaEiPakoSiirrot(std::vector<Siirto>& lista);
	bool tarkistaMustSuunnat();
	bool tarkistaMustSuunnat(Ruutu & ruutu);
	bool tarkistaValkSuunnat(Ruutu & ruutu);
	bool tarkistaValkSuunnat();
	void poistaMatittavat(std::vector<Siirto>& lista);
	bool poistaShakittavatSiirrot(Siirto & siirto);
	bool tarkistaMatti();
	bool poistaLaittomatKuningasSiirrot(std::vector<Siirto>& lista);
	bool tarkistaOhestaLyonti(std::vector<Siirto>& lista);
	bool tarkistaLinnoitus(std::vector<Siirto>& lista);
	bool onkoRuutuUhattuna(std::vector<Siirto> vastustajanlista, Ruutu & ruutu, int vari);
	bool onkoRuutuUhattuna(Ruutu & ruutu);
	std::vector<Siirto> ruudunUhkaajat(Ruutu& ruutu);
	int getSiirtovuoro();
	void setSiirtovuoro(int vari);
	bool getOnkoValkeaKuningasLiikkunut();
	bool getOnkoMustaKuningasLiikkunut();
	bool getOnkoValkeaDTliikkunut();
	bool getOnkoValkeaKTliikkunut();
	bool getOnkoMustaDTliikkunut();
	bool getOnkoMustaKTliikkunut();
	bool onkoMatti() { return matti; }
	bool lyoOhesta = false;
	Ruutu ohestaRuutu;

	

	const Sotilas* Asema::VS = new Sotilas(L"\u2659", VALK, 1);
	const Sotilas* Asema::MS = new Sotilas(L"\u265F", MUST, 2);
	const Kuningas* Asema::valkoinenKuningas = new Kuningas(L"\u2654", VALK, 3);
	const Kuningas* Asema::mustaKuningas = new Kuningas(L"\u265A", MUST, 4);
	const Kuningatar* Asema::VD = new Kuningatar(L"\u2655", VALK, 5);
	const Kuningatar* Asema::MD = new Kuningatar(L"\u265B", MUST, 6);
	const Torni* Asema::VT = new Torni(L"\u2656", VALK, 7);
	const Torni* Asema::MT = new Torni(L"\u265C", MUST, 8);
	const Ratsu* Asema::VR = new Ratsu(L"\u2658", VALK, 9);
	const Ratsu* Asema::MR = new Ratsu(L"\u265E", MUST, 10);
	const Lahetti* Asema::VL = new Lahetti(L"\u2657", VALK, 11);
	const Lahetti* Asema::ML = new Lahetti(L"\u265D", MUST, 12);
	const Nappula* Asema::tyhja = nullptr;
	const Nappula* palautaRuutuNappula(Ruutu & ruutu);

	bool shakkimatti = false;

	Ruutu valkoinenKuningasRuutu;
	Ruutu mustaKuningasRuutu;

private:

	Siirto viimeinenSiirto;
	const Nappula* alkuNappula;
	const Nappula* loppuNappula;
	void annaYksiSiirto(std::vector<Siirto> lista, Ruutu ruutu, int vari);
	void annaSiirrot(std::vector<Siirto>& lista, int vari);
	
	int siirtovuoro;
	bool onkoValkeaKuningasLiikkunut = false;
	bool onkoMustaKuningasLiikkunut = false;
	bool onkoValkeaDTliikkunut = false;
	bool onkoValkeaKTliikkunut = false;
	bool onkoMustaDTliikkunut = false;
	bool onkoMustaKTliikkunut = false;
	bool matti = false;

	double Qhaku(double alpha, double beta);

	double Qhaku(Asema & asema, double alpha, double beta);

};



