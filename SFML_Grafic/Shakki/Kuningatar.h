#pragma once
#include "Nappula.h"
#include "Lahetti.h"
#include "Torni.h"
class Kuningatar : public virtual Lahetti, public virtual Torni

{
	public:
	Kuningatar(std::wstring unicode, int vari, int nappula): Nappula(unicode, vari, nappula), Torni(unicode, vari, nappula), Lahetti(unicode, vari, nappula) {};
	void annaSiirrot(std::vector<Siirto>& lista, Ruutu* ruudunOsoitin, Asema* asemanOsoitin, int vari1) const {
		Lahetti::annaSiirrot(lista, ruudunOsoitin, asemanOsoitin, vari1);
		Torni::annaSiirrot(lista, ruudunOsoitin, asemanOsoitin, vari1);
	};
};

