#include "Lahetti.h"
#include "Asema.h"
#include "Siirto.h"

void Lahetti::annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulaSijainti, Asema* asema, int vari) const
{
	int alkuRivi = nappulaSijainti->rivi;
	int alkuSarake = nappulaSijainti->sar;
	int rivi, sar;
	rivi = alkuRivi + 1;
	sar = alkuSarake + 1;
	while (rivi < 8 && sar < 8)
	{
		if (asema->lauta[rivi][sar] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar)));
			rivi++;
			sar++;
		}
		else if (asema->lauta[rivi][sar]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar), true));
			break;
		}
		else
			break;
	}

	rivi = alkuRivi - 1;
	sar = alkuSarake - 1;
	while (rivi >=0 && sar >= 0)
	{
		if (asema->lauta[rivi][sar] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar)));
			rivi--;
			sar--;
		}
		else if (asema->lauta[rivi][sar]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar), true));
			break;
		}
		else
			break;
	}

	rivi = alkuRivi - 1;
	sar = alkuSarake + 1;
	while (rivi >= 0 && sar < 8)
	{
		if (asema->lauta[rivi][sar] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar)));
			rivi--;
			sar++;
		}
		else if (asema->lauta[rivi][sar]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar), true));
			break;
		}
		else
			break;
	}

	rivi = alkuRivi + 1;
	sar = alkuSarake - 1;
	while (rivi < 8 && sar >= 0)
	{
		if (asema->lauta[rivi][sar] == asema->tyhja)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar)));
			rivi++;
			sar--;
		}
		else if (asema->lauta[rivi][sar]->Vari() != vari)
		{
			lista.emplace_back(Siirto(Ruutu(alkuRivi, alkuSarake), Ruutu(rivi, sar), true));
			break;
		}
		else
			break;
	}
}
