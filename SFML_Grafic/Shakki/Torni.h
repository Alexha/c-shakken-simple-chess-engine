#pragma once
#include "Nappula.h"
class Torni :
	public virtual Nappula
{
public:
	Torni(std::wstring unicode, int vari, int nappula) : Nappula(unicode, vari, nappula) {

	};

	Torni() = default;
	void annaSiirrot(std::vector<Siirto>& lista, Ruutu*, Asema*, int vari) const;
};

