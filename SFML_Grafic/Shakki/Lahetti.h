#pragma once
#include "Nappula.h"

class Lahetti :
	public virtual Nappula
{
public:
	Lahetti(std::wstring unicode, int vari, int nappula): Nappula(unicode,vari,nappula) {
	};
	Lahetti() = default;
	void annaSiirrot(std::vector<Siirto>& lista, Ruutu*, Asema*, int vari) const;
	
};

