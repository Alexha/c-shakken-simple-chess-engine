#pragma once
class Ruutu {
public:
	Ruutu(int rivi, int sar) : sar(sar), rivi(rivi) {};
	Ruutu() : sar(0), rivi(0) {};
	int sar = 0;
	int rivi = 0;
	bool operator ==(const Ruutu& rhs) {
		return sar == rhs.sar && rivi == rhs.rivi;
	}

	bool operator !=(const Ruutu& rhs) {
		return !(*this == rhs);
	}

};
