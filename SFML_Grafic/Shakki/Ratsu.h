#pragma once
#include "Nappula.h"
static const int ratsuSiirrot[8][2] = {
	{ 1,-2 },
	{ 2,-1 },
	{ 2,1 },
	{ 1,2 },
	{ -1,2 },
	{ -2,1 },
	{ -2,-1 },
	{ -1,-2 }
};

class Ratsu :
	public Nappula
{
private:

public:
	Ratsu(std::wstring unicode, int vari, int nappula) {
		_unicode = unicode;
		_vari = vari;
		_koodi = nappula;
	};
	void annaSiirrot(std::vector<Siirto>& lista, Ruutu*, Asema*, int vari) const;
};

