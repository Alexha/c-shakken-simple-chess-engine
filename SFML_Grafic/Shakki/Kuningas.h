#pragma once
#include "Nappula.h"
class Ruutu;
class Asema;

class Kuningas :
	public Nappula
{
public:
	Kuningas(std::wstring unicode, int vari, int nappula) {
		_unicode = unicode;
		_vari = vari;
		_koodi = nappula;
	};
	void annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulanSijainti, Asema* asema, int vari) const ;
};

