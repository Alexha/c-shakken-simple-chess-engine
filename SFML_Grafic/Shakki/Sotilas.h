#pragma once
#include "Nappula.h"
#include <vector>
#include "Siirto.h"
//class Siirto;
class Sotilas :
	public Nappula
{
private:
	public:
	Sotilas(std::wstring unicode, int vari, int nappula) {
		_unicode = unicode;
		_vari = vari;
		_koodi = nappula;
	};

	static void tarkistaOhesta(std::vector<Siirto>& lista,Ruutu * nappulaSijainti, Asema * asema, int vari);

	void annaSiirrot(std::vector<Siirto>& lista, Ruutu*, Asema*, int vari) const;
};

