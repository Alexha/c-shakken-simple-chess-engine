#pragma once
#include <iostream>
#include <string>
#include <vector>

class Asema;
class Siirto;
class Ruutu;
// Vakioarvot nappulatyypeille.


enum
{
	VALK,
	MUST
};

class Nappula {

protected:
	std::wstring _unicode;
	int _vari; // valkea = 0, musta = 1
	int _koodi; // VT, VR, MT tms.

public:
	Nappula(std::wstring unicode, int vari, int nappula) : _unicode(unicode), _vari(vari), _koodi(nappula) {};
	Nappula() {};
	
	int Vari() const { return _vari; };
	int Koodi() const { return _koodi; };
	std::wstring Unicode() const { return _unicode; };
	virtual void annaSiirrot(std::vector<Siirto>& lista, Ruutu* nappulanSijainti, Asema* asema, int vari) const = 0;

};
