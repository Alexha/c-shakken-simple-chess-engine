#include "Kayttoliittyma.h"
#include <iostream>
#include <algorithm>
#include <string>
#include <io.h>
#include <fcntl.h>
//#include <windows.h>
#include "Siirto.h"
#include "..\Minimax.h"
#include "..\Ajastin.h"


Kayttoliittyma* Kayttoliittyma::instance = nullptr;


void Kayttoliittyma::vaihdaVuoro()
{
	if (asema->getSiirtovuoro() == Asema::VALK)
		asema->setSiirtovuoro(Asema::MUST);
	else
		asema->setSiirtovuoro(Asema::VALK);
	if (state == NORMAALI)
		state = AI_VUORO;
	else if (state == AI_VUORO)
		state = NORMAALI;
	if (vain_ai)
		state = AI_VUORO;
}

Siirto Kayttoliittyma::annaVastustajanSiirto(std::string syote)
{

	if (syote.at(0) == 'O')
	{
		size_t linnoitus = count(syote.begin(), syote.end(), '-');
		if (linnoitus < 0 || linnoitus > 2)
		{
			return Siirto(false, false);	// virhe;
		}
		else
		{
			if (linnoitus == 1) {

				return Siirto(true, false);
			}
			else {

				return Siirto(false, true);
			}
		}
	}
	size_t viiva = syote.find("-");
	if (viiva == std::string::npos)
	{
		return Siirto(false, false);	// virhe
	}

	Ruutu alku = Ruutu(syote.at(1) - 48 - 1, syote.at(0) - 96 - 1);  // -48 to get real numbers from ascii
	Ruutu loppu = Ruutu(syote.at(viiva + 2) - 48 - 1, syote.at(viiva + 1) - 96 - 1); // -48 to get real numbers from ascii

	return Siirto(alku, loppu);
}

bool Kayttoliittyma::korotaNappula(int nappula)
{

	if (state == KOROTUS)
	{
		switch (nappula)
		{
		case 10: // Daami, SF::keyboard::key

			if (asema->getSiirtovuoro() == Asema::MUST)
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->MD;
			else
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->VD;
			state = NORMAALI;
			
			return true;
			break;
		case 11: // lahetti, SF::keyboard::key
			if (asema->getSiirtovuoro() == Asema::MUST)
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->ML;
			else
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->VL;
			state = NORMAALI;
			return true;
			break;
		case 17: // ratsu, SF::keyboard:key
			if (asema->getSiirtovuoro() == Asema::MUST)
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->MR;
			else
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->VR;
			state = NORMAALI; 
			return true; break;
		case 19: // torni
			if (asema->getSiirtovuoro() == Asema::MUST)
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->MT;
			else
				asema->lauta[promotionRuutu.rivi][promotionRuutu.sar] = asema->VT;
			state = NORMAALI; 
			return true;
			break;
		default:
			return false;
			break;
		}
	}
}

void Kayttoliittyma::tekoalySiirto()
{
	const int depth = 4;


	Ajastin ajastin("tekoalysiirto");
	Siirto siirto;
	double arvo;
	siirtoArvo vastaus = tekoaly.parallel_arvaus(depth, *asema, VALK_HAVIO, MUST_HAVIO);
	siirto = vastaus.siirto;
	arvo = vastaus.arvo;
	if (siirto == Siirto(0, 0) && siirto.onkoLyhytLinna() == false && siirto.onkoPitkalinna() == false) // ei ollut oikeita siirtoja
	{
		peliOhi = true;
		std::cout << "\nTietokone luovuttaa, shakkimatti. Vari: " << asema->getSiirtovuoro();
	}
	if (arvo == VALK_HAVIO && asema->getSiirtovuoro() == Asema::VALK)
	{
		peliOhi = true;
		std::cout << "\nTietokone luovuttaa, tilanne toivoton. Vari: " << asema->getSiirtovuoro();
	}
	else if (arvo == MUST_HAVIO && asema->getSiirtovuoro() == Asema::MUST)
	{
		peliOhi = true;
		std::cout << "\nTietokone luovuttaa, tilanne toivoton. Vari: " << asema->getSiirtovuoro();
	}
	else
	{
		
		// T�m� on korotusta varten
		if (asema->getSiirtovuoro() == Asema::VALK)
		{
			if (asema->palautaRuutuNappula(siirto.getAlkuruutu()) == asema->VS
				&& siirto.getLoppuruutu().rivi == 7)
			{

				asema->paivitaAsema(&siirto);

				asema->lauta[siirto.getAlkuruutu().rivi][siirto.getLoppuruutu().sar] = asema->tyhja;
				asema->lauta[siirto.getLoppuruutu().rivi][siirto.getLoppuruutu().sar] = asema->VD;
			}
			else
				asema->paivitaAsema(&siirto);
		}
		else if (asema->getSiirtovuoro() == Asema::MUST)
		{
			if (asema->palautaRuutuNappula(siirto.getAlkuruutu()) == asema->MS
				&& siirto.getLoppuruutu().rivi == 0)
			{

				asema->paivitaAsema(&siirto);

				asema->lauta[siirto.getAlkuruutu().rivi][siirto.getLoppuruutu().sar] = asema->tyhja;
				asema->lauta[siirto.getLoppuruutu().rivi][siirto.getLoppuruutu().sar] = asema->MD;
			}
			else
				asema->paivitaAsema(&siirto);
		}

		std::cout << "\nTehty siirto:" << (char)(siirto.getAlkuruutu().sar + 'A')<< siirto.getAlkuruutu().rivi + 1 
			<< "-"<< (char)(siirto.getLoppuruutu().sar + 'A')   << siirto.getLoppuruutu().rivi + 1 <<" arvo: " << vastaus.arvo;

		vaihdaVuoro();
	}
}


bool Kayttoliittyma::lueSyote(std::string syote)
{

	Siirto siirto;
	if (state == NORMAALI)
	{
		siirto = annaVastustajanSiirto(syote);
		if (asema->getSiirtovuoro() == Asema::VALK && siirto.getLoppuruutu().rivi == 7
			&& asema->lauta[siirto.getAlkuruutu().rivi][siirto.getAlkuruutu().sar] == asema->VS)
		{
			state = KOROTUS;
			asema->lauta[siirto.getAlkuruutu().rivi][siirto.getAlkuruutu().sar] = asema->tyhja;
			promotionRuutu = siirto.getLoppuruutu();


		}
		else if (asema->getSiirtovuoro() == Asema::MUST && siirto.getLoppuruutu().rivi == 0
			&& asema->lauta[siirto.getAlkuruutu().rivi][siirto.getAlkuruutu().sar] == asema->MS)
		{
			state = KOROTUS;
			asema->lauta[siirto.getAlkuruutu().rivi][siirto.getAlkuruutu().sar] = asema->tyhja;
			promotionRuutu = siirto.getLoppuruutu();
			//input.setString("Korotus: (T) Torni, (R) Ratsu,\n (K) Kuningatar, (L) L�hetti");

		}
		else {
			std::vector<Siirto> lista;
			asema->annaLaillisetSiirrot(lista);
			if (std::find(lista.begin(), lista.end(), siirto) != lista.end())
			{
				asema->paivitaAsema(&siirto);

				asema->tarkistaMatti();
				vaihdaVuoro();
				return true;
			}
			else
				return false;
		}

	}
	return true;
}
Kayttoliittyma* Kayttoliittyma::getInstance()
{
	if (instance == 0) {
		instance = new Kayttoliittyma();
	}
	return instance;
}

