#include "Asema.h"
#include <iostream>
#include <algorithm>
#include <functional>
#include <vector>
#include <string>
#include "..\Ajastin.h"
#include "Siirto.h"


void Asema::luoLauta()
{

	// tyhj�t
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
			lauta[i][j] = tyhja;
	}


	/*// TESTI
	lauta[7][e] = mustaDaami;
	lauta[2][g] = mustaDaami; matti = true;
	lauta[1][e] = valkoinenDaami;
	lauta[0][e] = valkoinenKuningas;
	valkoinenKuningasRuutu = Ruutu(0, e);
	onkoValkeaKuningasLiikkunut = true;
	onkoMustaKuningasLiikkunut = true;
	// TESTI
	*/


	
	for (int i = 0; i < 8; i++)
	{
		// Sotilaat paikoilleen
		lauta[1][i] = VS;
		lauta[6][i] = MS;
	}

	// Tornit
	lauta[0][a] = lauta[0][h] = VT;
	lauta[7][a] = lauta[7][h] = MT;
	// Ratsut
	lauta[0][b] = lauta[0][g] = VR;
	lauta[7][b] = lauta[7][g] = MR;
	// L�hetit
	lauta[0][c] = lauta[0][f] = VL;
	lauta[7][c] = lauta[7][f] = ML;
	// Kuninkaalliset
	lauta[0][e] = valkoinenKuningas;
	lauta[7][e] = mustaKuningas;
	lauta[0][d] = VD;
	lauta[7][d] = MD;



	/// TESTEJ�

	// linnoitustesti
	//lauta[7][b] = tyhja;
	//lauta[7][c] = tyhja;
	//lauta[7][d] = tyhja;

	//lauta[7][f] = tyhja;
	//lauta[7][g] = tyhja;


	//lauta[0][b] = tyhja;
	//lauta[0][c] = tyhja;
	//lauta[0][d] = tyhja;
	//	  
	//lauta[0][f] = tyhja;
	//lauta[0][g] = tyhja;

	////lauta[7][h] = tyhja;
	////lauta[7][a] = tyhja;
	////lauta[0][a] = tyhja;
	////lauta[0][h] = tyhja;
	///////

	//// korotustesti
	//lauta[6][a] = VS;
	//lauta[6][b] = VS;

	mustaKuningasRuutu = Ruutu(7, e);
	valkoinenKuningasRuutu = Ruutu(0, e);


	// Valkoinen aloittaa
	siirtovuoro = vuoro::VALK;



	//// v��r� shakkimatti mustalle
	//siirtovuoro = vuoro::MUST;
	//for (int i = 0; i < 8; i++)
	//{
	//	for (int j = 0; j < 8; j++)
	//		lauta[i][j] = tyhja;
	//}

	//lauta[4][a] = MS;
	//lauta[1][a] = VS;
	//lauta[0][a] = VT;

	//lauta[7][b] = VL;
	//lauta[4][b] = VR;
	//lauta[1][b] = VS;

	//lauta[1][c] = VS;

	//lauta[6][d] = MS;
	//lauta[3][d] = VR;

	//lauta[7][e] = mustaKuningas;
	//lauta[6][e] = ML;
	//lauta[5][e] = MS;
	//lauta[4][e] = VS;
	//lauta[1][e] = VD;
	//lauta[0][e] = valkoinenKuningas;

	//lauta[6][f] = MS;
	//lauta[1][f] = VS;
	//lauta[5][g] = MS;


	//lauta[7][h] = MT;
	//lauta[5][h] = MS;
	//lauta[4][h] = VS;
	//lauta[2][h] = VS;
	//lauta[0][h] = VT;
	/////////////////////////////////////77

	 ///*korotustesti*/

	//for (int i = 0; i < 8; i++)
	//{
	//	for (int j = 0; j < 8; j++)
	//		lauta[i][j] = tyhja;
	//}                                                                                                                                                                                                                                                                                   

	//lauta[6][a] = VS;
	//lauta[6][b] = VS;


	//lauta[1][a] = MS;
	//lauta[1][b] = MS;
	/////7////////////

	

	

}
void Asema::paivitaAsema(Siirto* siirto) {
	if (siirto->getAlkuruutu() == mustaKuningasRuutu)
	{
		mustaKuningasRuutu = siirto->getLoppuruutu();
	}
	else if (siirto->getAlkuruutu() == valkoinenKuningasRuutu)
	{
		valkoinenKuningasRuutu = siirto->getLoppuruutu();
	}

	if (lyoOhesta)
	{
		if (lauta[siirto->getAlkuruutu().rivi][siirto->getAlkuruutu().sar] == VS
			|| lauta[siirto->getAlkuruutu().rivi][siirto->getAlkuruutu().sar] == MS)
		{
			if (siirtovuoro == MUST)
			{
				if (siirto->getLoppuruutu().rivi == ohestaRuutu.rivi - 1)
				{
					lauta[ohestaRuutu.rivi][ohestaRuutu.sar] = tyhja;
				}
			}
			else if (siirtovuoro == VALK)
			{
				if (siirto->getLoppuruutu().rivi == ohestaRuutu.rivi + 1)
				{
					lauta[ohestaRuutu.rivi][ohestaRuutu.sar] = tyhja;
				}
			}
		}
		lyoOhesta = false;
	}
	if (siirto->getAlkuruutu().rivi == 1 && siirto->getLoppuruutu().rivi == 3)
	{
		if (lauta[siirto->getAlkuruutu().rivi][siirto->getAlkuruutu().sar] == VS)
		{
			lyoOhesta = true;
			ohestaRuutu = siirto->getLoppuruutu();
		}
	}
	if (siirto->getAlkuruutu().rivi == 6 && siirto->getLoppuruutu().rivi == 4)
	{
		if (lauta[siirto->getAlkuruutu().rivi][siirto->getAlkuruutu().sar] == MS)
		{
			lyoOhesta = true;
			ohestaRuutu = siirto->getLoppuruutu();
		}
	}
	if (siirto->getAlkuruutu().rivi == 0 && siirto->getAlkuruutu().sar == e) {
		onkoValkeaKuningasLiikkunut = true;
	}
	else if (siirto->getAlkuruutu().rivi == 7 && siirto->getAlkuruutu().sar == e) {
		onkoMustaKuningasLiikkunut = true;
	}
	else if (siirto->getAlkuruutu().rivi == 0 && siirto->getAlkuruutu().sar == a) {
		onkoValkeaDTliikkunut = true;
	}
	else if (siirto->getAlkuruutu().rivi == 7 && siirto->getAlkuruutu().sar == a) {
		onkoMustaDTliikkunut = true;
	}
	else if (siirto->getAlkuruutu().rivi == 0 && siirto->getAlkuruutu().sar == h) {
		onkoValkeaKTliikkunut = true;
	}
	else if (siirto->getAlkuruutu().rivi == 7 && siirto->getAlkuruutu().sar == h) {
		onkoMustaKTliikkunut = true;
	}

	// Linnoitussiirrot
	if (siirto->onkoLyhytLinna()) {
		if (siirtovuoro == vuoro::VALK) {
			lauta[0][e] = tyhja;
			lauta[0][h] = tyhja;
			lauta[0][g] = valkoinenKuningas;
			lauta[0][f] = VT;
			valkoinenKuningasRuutu = Ruutu(0, g);
			onkoValkeaKuningasLiikkunut = true;
			return;
		}
		else if (siirtovuoro == vuoro::MUST)
		{
			lauta[7][e] = tyhja;
			lauta[7][h] = tyhja;
			lauta[7][g] = mustaKuningas;
			lauta[7][f] = MT;
			mustaKuningasRuutu = Ruutu(7, g);
			onkoMustaKuningasLiikkunut = true;
			return;
		}
	}
	else
		if (siirto->onkoPitkalinna()) {
			if (siirtovuoro == vuoro::VALK) {
				lauta[0][e] = tyhja;
				lauta[0][a] = tyhja;
				lauta[0][c] = valkoinenKuningas;
				lauta[0][d] = VT;
				valkoinenKuningasRuutu = Ruutu(0, c);
				onkoValkeaKuningasLiikkunut = true;
				return;
			}
			else if (siirtovuoro == vuoro::MUST)
			{
				lauta[7][e] = tyhja;
				lauta[7][a] = tyhja;
				lauta[7][c] = mustaKuningas;
				lauta[7][d] = MT;
				mustaKuningasRuutu = Ruutu(7, c);
				onkoMustaKuningasLiikkunut = true;
				return;
			}
		}

		int alkuSarake = siirto->getAlkuruutu().sar;
		int alkuRivi = siirto->getAlkuruutu().rivi;
		int loppuSarake = siirto->getLoppuruutu().sar;
		int loppuRivi = siirto->getLoppuruutu().rivi;


		const Nappula* nappula = lauta[alkuRivi][alkuSarake];

		lauta[loppuRivi][loppuSarake] = nappula;
		lauta[alkuRivi][alkuSarake] = tyhja;
		
		

}
void Asema::annaLaillisetSiirrot(std::vector<Siirto>& lista)
{
	
	if (shakkimatti)
		return;
	annaSiirrot(lista, siirtovuoro);
	tarkistaLinnoitus(lista);
	if (lyoOhesta)
	{
		tarkistaOhestaLyonti(lista);
	}

	/*if (matti)
	{
		poistaEiPakoSiirrot(lista);
	}*/
	//poistaLaittomatKuningasSiirrot(lista);

	/*lista.remove_if(std::bind1st(std::mem_fun(&Asema::poistaShakittavatSiirrot), this));*/
	
	poistaMatittavat(lista);
	if (lista.size() == 0)
		shakkimatti = true;
	//tarkistaMatti();
}

void Asema::poistaEiPakoSiirrot(std::vector<Siirto>& lista)
{
	std::remove_if(lista.begin(), lista.end(),
		[this](Siirto& siirto) {

		Asema testiAsema = *this;
		testiAsema.paivitaAsema(&siirto);
		if (getSiirtovuoro() == VALK)
		{
			if (testiAsema.onkoRuutuUhattuna(testiAsema.valkoinenKuningasRuutu))
				return true;
		}
		else
		{
				if (testiAsema.onkoRuutuUhattuna(testiAsema.mustaKuningasRuutu))
					return true;
		}
	}
	);
}


bool Asema::tarkistaMustSuunnat()
{
	int alkuSar = mustaKuningasRuutu.sar;
	int alkuRiv = mustaKuningasRuutu.rivi;

	// katso pysty
	for (int i = alkuRiv+1; i < 8; i++)
	{
		if (lauta[i][alkuSar] == VD || lauta[i][alkuSar] == VT
			|| lauta[alkuRiv+1][alkuSar] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[i][alkuSar] != tyhja)
			break;
	}
	// katso ala
	for (int i = alkuRiv-1; i > 0; i--)
	{
		if (lauta[i][alkuSar] == VD || lauta[i][alkuSar] == VT
			|| lauta[alkuRiv-1][alkuSar] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[i][alkuSar] != tyhja)
			break;
	}
	// katso oikea
	for (int i = alkuSar+1; i < 8; i++)
	{
		if (lauta[alkuRiv][i] == VD || lauta[alkuRiv][i] == VT
			|| lauta[alkuRiv][alkuSar+1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[alkuRiv][i] != tyhja)
			break;
	}
	// katso vasen
	for (int i = alkuSar-1; i > 0; i--)
	{
		if (lauta[alkuRiv][i] == VD || lauta[alkuRiv][i] == VD
			|| lauta[alkuRiv][alkuSar-1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[alkuRiv][i] != tyhja)
			break;
	}

	// katso yl�oikeaviisto
	int rivi = alkuRiv;
	int sar = alkuSar;
	while (rivi < 7 && sar < 7)
	{

		rivi++;
		sar++;
		if (lauta[rivi][sar] == VD || lauta[rivi][sar] == VL
			|| lauta[alkuRiv+1][alkuSar+1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso alaoikeaviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi > 0 && sar < 7)
	{

		rivi--;
		sar++;
		if (lauta[rivi][sar] == VD || lauta[rivi][sar] == VL
			|| lauta[alkuRiv-1][alkuSar+1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso alavasenviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi > 0 && sar > 0)
	{

		rivi--;
		sar--;
		if (lauta[rivi][sar] == VD || lauta[rivi][sar] == VL
			|| lauta[alkuRiv -1][alkuSar -1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso ylavasenviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi < 7 && sar > 0)
	{

		rivi++;
		sar--;
		if (lauta[rivi][sar] == VD || lauta[rivi][sar] == VL || lauta[alkuRiv+1][alkuSar +1] == valkoinenKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// voiko sotilas sy�d�?
	// huomaa alkurivi - 1 (sotilasvoi olla vain alapuolella
	if (alkuRiv > 0 && alkuSar < 7)
		if (lauta[alkuRiv - 1][alkuSar + 1] == VS)
		{
			return true;
		}
	if (alkuRiv > 0 && alkuSar > 0)
		if (lauta[alkuRiv - 1][alkuSar - 1] == VS)
		{
			return true;
		}



	// tarkista viel� hevoset
	for (int i = 0; i < 8; i++)
	{
		int i_rivi = alkuRiv + ratsuSiirrot[i][0];
		int i_sar = alkuSar + ratsuSiirrot[i][1];
		if (i_rivi >= 0 && i_sar >= 0 &&
			i_rivi < 8 && i_sar < 8)
		{
			if (lauta[i_rivi][i_sar] == VR)
				return true;
		}

	}
	// ei l�ytynyt mit��n!
	return false;
}

bool Asema::tarkistaValkSuunnat()
{
	int alkuSar = valkoinenKuningasRuutu.sar;
	int alkuRiv = valkoinenKuningasRuutu.rivi;

	// katso pysty
	for (int i = alkuRiv+1; i < 8; i++)
	{
		if (lauta[i][alkuSar] == MD || lauta[i][alkuSar] == MT
			|| lauta[alkuRiv+1][alkuSar] == mustaKuningas)
		{
			return true;
		}
		if (lauta[i][alkuSar] != tyhja)
			break;
	}
	// katso ala
	for (int i = alkuRiv-1; i > 0; i--)
	{
		if (lauta[i][alkuSar] == MD || lauta[i][alkuSar] == MT
			|| lauta[alkuRiv-1][alkuSar] == mustaKuningas)
		{
			return true;
		}
		if (lauta[i][alkuSar] != tyhja)
			break;
	}
	// katso oikea
	for (int i = alkuSar+1; i < 8; i++)
	{
		if (lauta[alkuRiv][i] == MD || lauta[alkuRiv][i] == MT
			|| lauta[alkuRiv][alkuSar+1] == mustaKuningas)
		{
			return true;
		}
		if (lauta[alkuRiv][i] != tyhja)
			break;
	}
	// katso vasen
	for (int i = alkuSar-1; i >0; i--)
	{
		if (lauta[alkuRiv][i] == MD || lauta[alkuRiv][i] == MT
			|| lauta[alkuRiv][alkuSar-1] == mustaKuningas)
		{
			return true;
		}
		if (lauta[alkuRiv][i] != tyhja)
			break;
	}

	// katso yl�oikeaviisto
	int rivi = alkuRiv;
	int sar = alkuSar;
	while (rivi < 7 && sar < 7)
	{

		rivi++;
		sar++;
		if (lauta[rivi][sar] == MD || lauta[rivi][sar] == ML
			|| lauta[alkuRiv+1][alkuSar+1] == mustaKuningas) // ainoa suunta miss� voi olla sotilas
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso alaoikeaviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi > 0 && sar < 7)
	{

		rivi--;
		sar++;
		if (lauta[rivi][sar] == MD || lauta[rivi][sar] == ML
			|| lauta[alkuRiv -1][alkuSar + 1] == mustaKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso alavasenviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi > 0 && sar > 0)
	{

		rivi--;
		sar--;
		if (lauta[rivi][sar] == MD || lauta[rivi][sar] == ML
			|| lauta[alkuRiv - 1][alkuSar - 1] == mustaKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// katso ylavasenviisto
	rivi = alkuRiv;
	sar = alkuSar;
	while (rivi < 7 && sar > 0)
	{

		rivi++;
		sar--;
		if (lauta[rivi][sar] == MD || lauta[rivi][sar] == ML
			|| lauta[alkuRiv + 1][alkuSar - 1] == mustaKuningas)
		{
			return true;
		}
		if (lauta[rivi][sar] != tyhja)
			break;
	}

	// voiko sotilas sy�d�?
	// huomaa alkurivi + 1 (sotilasvoi olla vain ylapuolella
	if(alkuRiv < 8 && alkuSar < 8)
	if (lauta[alkuRiv + 1][alkuSar + 1] == MS)
	{
		return true;
	}
	if (alkuRiv > 0 && alkuSar > 0)
	if (lauta[alkuRiv + 1][alkuSar - 1] == MS)
	{
		return true;
	}

	// tarkista viel� hevoset
	for (int i = 0; i < 8; i++)
	{
		int i_rivi = alkuRiv + ratsuSiirrot[i][0];
		int i_sar = alkuSar + ratsuSiirrot[i][1];
		if (i_rivi >= 0 && i_sar >= 0 &&
			i_rivi < 8 && i_sar < 8)
		{
			if (lauta[i_rivi][i_sar] == MR)
				return true;
		}

	}
	// ei l�ytynyt mit��n!
	return false;
}

void Asema::poistaMatittavat(std::vector<Siirto>& lista)
{

	Asema testiAsema = *this;

	auto siirto = lista.begin();
	while (siirto != lista.end())
	{

		testiAsema = *this;
		testiAsema.paivitaAsema(&*siirto);
		if (siirtovuoro == VALK)
		{
			if(testiAsema.tarkistaValkSuunnat())
				siirto = lista.erase(siirto);
			else
				++siirto;
		}
		else
			if (testiAsema.tarkistaMustSuunnat())
				siirto = lista.erase(siirto);
			else
				++siirto;
	}
	/*std::vector<Siirto> vastustajanlista;
	auto siirto = lista.begin(); 
	while(siirto != lista.end())
	{
			vastustajanlista.clear();
			testiAsema = *this;
			testiAsema.paivitaAsema(&*siirto);
			
			
			if (getSiirtovuoro() == VALK)
			{
				testiAsema.annaSiirrot(vastustajanlista, MUST);
				if (testiAsema.onkoRuutuUhattuna(vastustajanlista, testiAsema.valkoinenKuningasRuutu, VALK))
					siirto = lista.erase(siirto);
				else
					++siirto;
			}
			else
			{
				testiAsema.annaSiirrot(vastustajanlista, VALK);
				if (testiAsema.onkoRuutuUhattuna(vastustajanlista, testiAsema.mustaKuningasRuutu, MUST))
					siirto =lista.erase(siirto);
				else
					++siirto;
			}
	}*/
}


bool Asema::tarkistaMatti()
{
	
	if(onkoRuutuUhattuna(mustaKuningasRuutu))
	{
		matti = true;
		return true;
	
	}
	if (onkoRuutuUhattuna(valkoinenKuningasRuutu))
	{
		matti = true;
		return true;

	}
	matti = false;
	return false;
}


bool Asema::tarkistaLinnoitus(std::vector<Siirto>& lista)
{
	if (siirtovuoro == VALK)
	{
		if (onkoValkeaKuningasLiikkunut)
			return false;
		if (!onkoValkeaKTliikkunut)
		{
			// tarkista onko ruudut tyhji�
			if (lauta[0][f] == tyhja &&
				lauta[0][g] == tyhja &&
				lauta[0][h] == VT)
			{// ovatko ruudut uhattuina?
				if (!onkoRuutuUhattuna(Ruutu(7, h))
					&&!onkoRuutuUhattuna(Ruutu(0, f))
					&& !onkoRuutuUhattuna(Ruutu(0, g)))
				{
					lista.push_back(Siirto(true, false));
				}
			}
		}
		if (!onkoValkeaDTliikkunut)
		{
			// tarkista onko ruudut tyhji�
			if (lauta[0][a] == VT &&
				lauta[0][b] == tyhja &&
				lauta[0][c] == tyhja &&
				lauta[0][d] == tyhja)
			{// ovatko ruudut uhattuina?
				if (!onkoRuutuUhattuna(Ruutu(7, a))
					&&!onkoRuutuUhattuna(Ruutu(0, b))
					&& !onkoRuutuUhattuna(Ruutu(0, c))
					&& !onkoRuutuUhattuna(Ruutu(0, d)))
				{
					lista.push_back(Siirto(false, true));
				}
			}
		}
	}
	else if (siirtovuoro == MUST)
	{
		if (onkoMustaKuningasLiikkunut)
			return false;
		if (!onkoMustaKTliikkunut)
		{
			// tarkista onko ruudut tyhji�
			if (lauta[7][f] == tyhja &&
				lauta[7][g] == tyhja &&
				lauta[7][h] == MT)
			{// ovatko ruudut uhattuina?
				if (!onkoRuutuUhattuna(Ruutu(7, f))
					&& !onkoRuutuUhattuna(Ruutu(7, g))
					&& !onkoRuutuUhattuna(Ruutu(7, h)))
				{
					lista.push_back(Siirto(true, false));
				}
			}
		}
		if (!onkoMustaDTliikkunut)
		{
			// tarkista onko ruudut tyhji�
			if (lauta[7][a] == MT &&
				lauta[7][b] == tyhja &&
				lauta[7][c] == tyhja &&
				lauta[7][d] == tyhja)
			{// ovatko ruudut uhattuina?
				if (!onkoRuutuUhattuna(Ruutu(7, a)) &&
					!onkoRuutuUhattuna(Ruutu(7, b))
					&& !onkoRuutuUhattuna(Ruutu(7, c))
					&& !onkoRuutuUhattuna(Ruutu(7, d)))
				{
					lista.push_back(Siirto(false, true));
				}
			}
		}
		
	}
	return true;
}

bool Asema::onkoRuutuUhattuna(std::vector<Siirto> vastustajanlista, Ruutu& ruutu, int vari)
{
	
	//if (siirtovuoro == VALK) {
	//	annaSiirrot(vastustajanlista, MUST);
	//}
	//else if (siirtovuoro == MUST) {
	//	annaSiirrot(vastustajanlista, VALK);
	//}

	for (auto&& siirto : vastustajanlista)
	{
		if (siirto.getLoppuruutu() == ruutu)
			return true;
	}
	return false;
}

bool Asema::onkoRuutuUhattuna(Ruutu& ruutu)
{
	std::vector<Siirto> vastustajanlista;
	if (siirtovuoro == VALK) {
		annaSiirrot(vastustajanlista, MUST);
	}
	else if (siirtovuoro == MUST) {
		annaSiirrot(vastustajanlista, VALK);
	}

	for (auto&& siirto : vastustajanlista)
	{
		if (siirto.getLoppuruutu() == ruutu)
			return true;
	}
	return false;
}

int Asema::getSiirtovuoro() {
	return siirtovuoro;
}
void Asema::setSiirtovuoro(int vari) {
	siirtovuoro = vari;
}
bool Asema::getOnkoValkeaKuningasLiikkunut() {
	return onkoValkeaKuningasLiikkunut;
}
bool Asema::getOnkoMustaKuningasLiikkunut() {
	return onkoMustaKuningasLiikkunut;
}
bool Asema::getOnkoValkeaDTliikkunut() {
	return onkoValkeaDTliikkunut;
}
bool Asema::getOnkoValkeaKTliikkunut() {
	return onkoValkeaKTliikkunut;
}
bool Asema::getOnkoMustaDTliikkunut() {
	return onkoMustaDTliikkunut;
}
bool Asema::getOnkoMustaKTliikkunut() {
	return onkoMustaKTliikkunut;
}



bool Asema::tarkistaOhestaLyonti(std::vector<Siirto>& lista)
{

	int alkuRivi = ohestaRuutu.rivi;
	int alkuSarake = ohestaRuutu.sar;
	if (siirtovuoro == MUST && alkuRivi == 3)
	{
		if (lauta[alkuRivi][alkuSarake + 1] != tyhja) {
			if (lauta[alkuRivi][alkuSarake + 1]->Vari() == siirtovuoro) {
				lista.push_back(Siirto(Ruutu(alkuRivi, alkuSarake + 1), Ruutu(alkuRivi - 1, alkuSarake)));
			}
		}
		if (lauta[alkuRivi][alkuSarake - 1] != tyhja) {
			if (lauta[alkuRivi][alkuSarake - 1]->Vari() == siirtovuoro) {
				lista.push_back(Siirto(Ruutu(alkuRivi, alkuSarake - 1), Ruutu(alkuRivi - 1, alkuSarake)));
			}
		}
	}
	else if (getSiirtovuoro() == VALK && alkuRivi == 4)
	{
		if (lauta[alkuRivi][alkuSarake - 1] != tyhja) {
			if (lauta[alkuRivi][alkuSarake - 1]->Vari() == siirtovuoro) {
				lista.push_back(Siirto(Ruutu(alkuRivi, alkuSarake - 1), Ruutu(alkuRivi + 1, alkuSarake)));
			}
		}
		if (lauta[alkuRivi][alkuSarake + 1] != tyhja) {
			if (lauta[alkuRivi][alkuSarake + 1]->Vari() == siirtovuoro) {
				lista.push_back(Siirto(Ruutu(alkuRivi, alkuSarake + 1), Ruutu(alkuRivi + 1, alkuSarake)));
			}
		}
	}
	return true;
}


void Asema::annaYksiSiirto(std::vector<Siirto> lista, Ruutu ruutu, int vari)
{
	int i = ruutu.rivi;
	int k = ruutu.sar;

	lauta[i][k]->annaSiirrot(lista, &ruutu, this, vari);

}

void Asema::annaSiirrot(std::vector<Siirto>& lista, int vari)
{
	Ruutu ruutu;
	for (int i = 0; i < 8; i++)
	{
		ruutu.rivi = i;
		for (int k = 0; k < 8; k++)
		{
			ruutu.sar = k;
			if (lauta[i][k] != tyhja && lauta[i][k]->Vari() == vari)
				lauta[i][k]->annaSiirrot(lista, &ruutu, this, vari);
		}
	}
}

const Nappula* Asema::palautaRuutuNappula(Ruutu& ruutu)
{
	return lauta[ruutu.rivi][ruutu.sar];
}
