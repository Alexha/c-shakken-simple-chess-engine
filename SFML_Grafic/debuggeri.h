#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include <locale>
#include <codecvt>

#include <imgui.h>
#include <imgui-SFML.h>

#include "Shakki\Asema.h"

#define CELL_SZ 60

class Debuggeri
{
private:
	Asema* _asema;
	std::vector<sf::RectangleShape> debug_siirto_graphics;
	std::vector<sf::Shape*> debug_graphics;
	void push_move(Siirto& siirto);
public:
	Debuggeri(Asema* asema) : _asema(asema) {};
	void debug_piece_moves();
	void draw_debug(sf::RenderWindow& window);
};

void Debuggeri::debug_piece_moves()
{
	if (_asema->getSiirtovuoro() == VALK)
		ImGui::Text("Vuoro: Valkoinen");
	else
		ImGui::Text("Vuoro: Musta");

	ImGui::Value("Ohestalyonti: ", _asema->lyoOhesta);
	if (_asema->lyoOhesta)
	{
		ImGui::SameLine();
		std::string ohesta = "";
		ohesta += (char)_asema->ohestaRuutu.sar + 'A';
		ohesta += std::to_string(_asema->ohestaRuutu.rivi + 1);
		ImGui::Text(ohesta.c_str());
	}

	if (ImGui::CollapsingHeader("Lailliset Siirrot"))
	{
		if (ImGui::CollapsingHeader("Onko liikkunut"))
		{
			if (_asema->getSiirtovuoro() == VALK)
			{
				ImGui::Value("Kuningas:", _asema->getOnkoValkeaKuningasLiikkunut());
				ImGui::Value("KT: ", _asema->getOnkoValkeaKTliikkunut());
				ImGui::Value("DT: ", _asema->getOnkoValkeaDTliikkunut());
			}
			else
			{
				ImGui::Value("Kuningas: ", _asema->getOnkoMustaKuningasLiikkunut());
				ImGui::Value("KT: ", _asema->getOnkoMustaKTliikkunut());
				ImGui::Value("DT: ", _asema->getOnkoMustaDTliikkunut());
			}
		}
		if (ImGui::TreeNode("Pelaajan siirrot"))
		{
			std::vector<Siirto> siirrot;
			_asema->annaLaillisetSiirrot(siirrot);
			for (auto&& siirto : siirrot)
			{
				if (siirto.onkoLyhytLinna())
				{
					ImGui::Selectable("O-O");
					if (ImGui::IsItemHovered())
					{
						if(_asema->getSiirtovuoro() == Asema::VALK)
							push_move(Siirto(Ruutu(0,Asema::f), Ruutu(0,Asema::g)));
						else
							push_move(Siirto(Ruutu(7, Asema::f), Ruutu(7, Asema::g)));
					}
				}
				else if (siirto.onkoPitkalinna())
				{
					ImGui::Selectable("O-O-O");
					if (ImGui::IsItemHovered())
					{
						if (_asema->getSiirtovuoro() == Asema::VALK)
							push_move(Siirto(Ruutu(0, Asema::c), Ruutu(0, Asema::d)));
						else
							push_move(Siirto(Ruutu(7, Asema::c), Ruutu(7, Asema::d)));
					}
				}
				else
				{
					int alku_rivi = siirto.getAlkuruutu().rivi;
					int alku_sar = siirto.getAlkuruutu().sar;

					// Imgui Text ei tue suoraan wstring, käännetään utf-8
					std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> convert;
					std::wstring nappula_pitka = _asema->lauta[alku_rivi][alku_sar]->Unicode();

					std::string teksti;
					teksti += (char)alku_sar + 'A';
					teksti += std::to_string(alku_rivi + 1);
					teksti += "-";
					teksti += (char)(siirto.getLoppuruutu().sar + 'A');
					teksti += std::to_string(siirto.getLoppuruutu().rivi + 1);
					ImGui::Text(convert.to_bytes(nappula_pitka).c_str());
					ImGui::SameLine();
					ImGui::Selectable(teksti.c_str());
					if (ImGui::IsItemHovered())
					{
						push_move(siirto);
					}
				}
			}
			ImGui::TreePop();
		}
	}
}

void Debuggeri::draw_debug(sf::RenderWindow& window)
{
	//for (auto&& graph : debug_graphics)
	//{
	//	window.draw(*graph);
	//}

	for (auto&& graph : debug_siirto_graphics)
	{
		window.draw(graph);
	}
}

void Debuggeri::push_move(Siirto& siirto)
{
	debug_siirto_graphics.clear();
	int alku_rivi = siirto.getAlkuruutu().rivi;
	int alku_sar = siirto.getAlkuruutu().sar;
	int loppu_rivi = siirto.getLoppuruutu().rivi;
	int loppu_sar = siirto.getLoppuruutu().sar;
	debug_siirto_graphics.push_back(sf::RectangleShape(sf::Vector2f(CELL_SZ, CELL_SZ)));
	debug_siirto_graphics[0].setFillColor(sf::Color(200, 128, 128, 128));
	debug_siirto_graphics[0].setPosition(sf::Vector2f(CELL_SZ*loppu_sar, 7* CELL_SZ-CELL_SZ*loppu_rivi));

	debug_siirto_graphics.push_back(sf::RectangleShape(sf::Vector2f(CELL_SZ, CELL_SZ)));
	debug_siirto_graphics[1].setFillColor(sf::Color(128, 128, 200, 128));
	debug_siirto_graphics[1].setPosition(sf::Vector2f(CELL_SZ*alku_sar, 7 * CELL_SZ - CELL_SZ*alku_rivi));
}