//#pragma once
#include "Minimax.h"
#include <numeric>
#include <algorithm>
#include <thread>
#include <future>
#include <utility>
#include <iterator>
#include "Ajastin.h"



double Minimax::laskeMateriaali(Asema& asema)
{
	double score = 0;

	int mnappulat = 0;
	int vnappulat = 0;

	int vlahetteja = 0;
	int mlahetteja = 0;

	int vtornipari = 0;
	int mtornipari = 0;


	int vkuningatar = 0;
	int mkuningatar = 0;

	int vratsu = 0;
	int mratsu = 0;

	int vpawns = 0;
	int mpawns = 0;

	for (int i = 0; i < 8; i++)
	{

		for (int k = 0; k < 8; k++)
		{
			if (asema.lauta[i][k] == asema.VD)
			{
				vnappulat++;
				vkuningatar++;
				score += DAAMI_ARVO;
			}
			else if (asema.lauta[i][k] == asema.VT)
			{
				score += TORNI_ARVO;

				vnappulat++;
				if (++vtornipari >= 2)
					score += TORNI_PARI;
			}
			else if (asema.lauta[i][k] == asema.VL)
			{

				vnappulat++;
				score += LAHETTI_ARVO;
				vlahetteja++;
				if (vlahetteja >= 2)// bonus lahettiparista
					score += LAHETTI_PARI;
			}
			else if (asema.lauta[i][k] == asema.VR)
			{

				vnappulat++;
				vratsu++;
				if (vratsu >= 2)
					score += RATSU_PARI;
				score += RATSU_ARVO;
			}
			else if (asema.lauta[i][k] == asema.VS)

			{
				if (i == 7) // onko korotus
					score += DAAMI_ARVO;
				else
					score += SOTILAS_ARVO;


				vnappulat++;
				vpawns++;
			}
			else if (asema.lauta[i][k] == asema.valkoinenKuningas)
			{
				score += KUNINGAS_ARVO;
			}



			else if (asema.lauta[i][k] == asema.MD)
			{

				mnappulat++;
				mkuningatar++;
				score -= DAAMI_ARVO;
			}
			else if (asema.lauta[i][k] == asema.MT)
			{

				mnappulat++;
				score -= TORNI_ARVO;
				if (++mtornipari >= 2)
					score -= TORNI_PARI;
			}
			else if (asema.lauta[i][k] == asema.ML)
			{
				mnappulat++;
				score -= LAHETTI_ARVO;
				mlahetteja++;
				if (mlahetteja >= 2) // bonus lahettiparista
					score -= LAHETTI_PARI;
			}
			else if (asema.lauta[i][k] == asema.MR)
			{
				mnappulat++;
				mratsu++;
				score -= RATSU_ARVO;
				if (mratsu >= 2)
				{
					score -= RATSU_PARI;
				}
			}
			else if (asema.lauta[i][k] == asema.MS)
			{
				mnappulat++;
				mpawns++;
				if (i == 0)
					score -= DAAMI_ARVO;
				else
					score -= SOTILAS_ARVO;
			}
			else if (asema.lauta[i][k] == asema.mustaKuningas)
			{
				score -= KUNINGAS_ARVO;
			}
		}
	}


/*	// tarkista onko materiaalia voittamiseen
	if (mnappulat < 2 && mpawns == 0 && mkuningatar == 0 && mtornipari == 0
		&& (mlahetteja < 2 && mratsu == 0))
	{
		return MUST_HAVIO;
	}

	if (vnappulat < 2 && vpawns == 0 && vkuningatar == 0 && vtornipari == 0
		&& (vlahetteja < 2 && vratsu == 0))
	{
		return VALK_HAVIO;
	}*/


	return score;
}

double Minimax::laskePositio(Asema & asema)
{
	double score = 0;
	for (int i = 0; i < 8; i++)
	{
		for (int k = 0; k < 8; k++)
		{
			if (asema.lauta[i][k] == asema.VS)
			{
				score += sotilaat[63-(8 * i +7- k)];
			}
			else if (asema.lauta[i][k] == asema.MS)
			{
				score -= sotilaat[8 * i + k];
			}
			else if (asema.lauta[i][k] == asema.VR)
			{
				score += ratsut[63 - (8 * i + 7 - k)];
			}
			else if (asema.lauta[i][k] == asema.MR)
			{
				score -= ratsut[8 * i + k];
			}
			else if (asema.lauta[i][k] == asema.VL)
			{
				score += lahetit[63 - (8 * i + 7 - k)];
			}
			else if (asema.lauta[i][k] == asema.ML)
			{
				score -= lahetit[8 * i + k];
			}
			else if (asema.lauta[i][k] == asema.valkoinenKuningas)
			{
				score += kuningas[63 - (8 * i + 7 - k)];
			}
			else if (asema.lauta[i][k] == asema.mustaKuningas)
			{
				score -= kuningas[8 * i + k];
			}
			else if (asema.lauta[i][k] == asema.VT)
			{
				score += torni[63 - (8 * i + 7 - k)];
			}
			else if (asema.lauta[i][k] == asema.MT)
			{
				score -= torni[8 * i + k];
			}

			else if (asema.lauta[i][k] == asema.VD)
			{
				score += kuningatar[63 - (8 * i + 7 - k)];
			}
			else if (asema.lauta[i][k] == asema.VD)
			{
				score -= kuningatar[8 * i + k];
			}
		}
	}
	return score;
}

double Minimax::estetytNappulat(Asema& asema) {

	double score = 0;

	if (asema.palautaRuutuNappula(Ruutu(0, Asema::c)) == asema.VL
		&& asema.palautaRuutuNappula(Ruutu(1, Asema::d)) == asema.VS
		&& asema.palautaRuutuNappula(Ruutu(1, Asema::d)) != asema.tyhja)
		score -= KESKINAPPULA_ESTA;
	if (asema.palautaRuutuNappula(Ruutu(0, Asema::f)) == asema.VL
		&& asema.palautaRuutuNappula(Ruutu(1, Asema::e)) == asema.VS
		&& asema.palautaRuutuNappula(Ruutu(1, Asema::e)) != asema.tyhja)
		score -= KESKINAPPULA_ESTA;

	if (asema.palautaRuutuNappula(Ruutu(7, Asema::c)) == asema.ML
		&& asema.palautaRuutuNappula(Ruutu(6, Asema::d)) == asema.MS
		&& asema.palautaRuutuNappula(Ruutu(6, Asema::d)) != asema.tyhja)
		score += KESKINAPPULA_ESTA;
	if (asema.palautaRuutuNappula(Ruutu(7, Asema::f)) == asema.ML
		&& asema.palautaRuutuNappula(Ruutu(6, Asema::e)) == asema.MS
		&& asema.palautaRuutuNappula(Ruutu(6, Asema::e)) != asema.tyhja)
		score += KESKINAPPULA_ESTA;


	if ((asema.palautaRuutuNappula(Ruutu(0, Asema::f)) == asema.valkoinenKuningas
		|| asema.palautaRuutuNappula(Ruutu(0, Asema::g)) == asema.valkoinenKuningas)
		&& (asema.palautaRuutuNappula(Ruutu(0, Asema::h)) == asema.VT
			|| asema.palautaRuutuNappula(Ruutu(0, Asema::g)) == asema.VT))
		score -= KUNINGAS_ESTA_TORNIA;
	if ((asema.palautaRuutuNappula(Ruutu(0, Asema::c)) == asema.valkoinenKuningas
		|| asema.palautaRuutuNappula(Ruutu(0, Asema::b)) == asema.valkoinenKuningas)
		&& (asema.palautaRuutuNappula(Ruutu(0, Asema::a)) == asema.VT
			|| asema.palautaRuutuNappula(Ruutu(0, Asema::b)) == asema.VT))
		score -= KUNINGAS_ESTA_TORNIA;
	if ((asema.palautaRuutuNappula(Ruutu(7, Asema::f)) == asema.mustaKuningas
		|| asema.palautaRuutuNappula(Ruutu(7, Asema::g)) == asema.mustaKuningas)
		&& (asema.palautaRuutuNappula(Ruutu(7, Asema::h)) == asema.MT
			|| asema.palautaRuutuNappula(Ruutu(7, Asema::g)) == asema.MT))
		score += KUNINGAS_ESTA_TORNIA;
	if ((asema.palautaRuutuNappula(Ruutu(7, Asema::c)) == asema.mustaKuningas
		|| asema.palautaRuutuNappula(Ruutu(7, Asema::b)) == asema.mustaKuningas)
		&& (asema.palautaRuutuNappula(Ruutu(7, Asema::a)) == asema.MT
			|| asema.palautaRuutuNappula(Ruutu(7, Asema::b)) == asema.MT))
		score += KUNINGAS_ESTA_TORNIA;

	return score;
}


double Minimax::evaluate(Asema& asema)
{
	double score = 0;


	double materiaali = laskeMateriaali(asema);

	// materiaali ei riit� voittamiseen
	if (materiaali == MUST_HAVIO || materiaali == VALK_HAVIO)
		return materiaali;
	else
		score += materiaali;
	score += laskePositio(asema);
	score += estetytNappulat(asema);
	return score;
}


double Minimax::valkoinen(const int depth, Asema asema, double alpha, double beta)
{
	int syvyys = depth;
	std::vector<Siirto>siirrot;
	asema.annaLaillisetSiirrot(siirrot);

	if (depth <= 0)
	{
		std::vector<Siirto> syovat_siirrot;
		double aseman_arvo_erotus = alpha - evaluate(asema);
		std::copy_if(siirrot.begin(), siirrot.end(), std::back_inserter(syovat_siirrot), [](Siirto& siirto) {
			return siirto.syo;
		});

		if (syovat_siirrot.size() == 0)
			return evaluate(asema);
		else
		{
			double syontiSarjanArvo = 0;
			Siirto syontiSarja;
			for (auto&& siirto : syovat_siirrot)
			{
				double lyonninArvo = syontiSarjaEvaluointi(siirto, asema, Asema::VALK);
				if (lyonninArvo > syontiSarjanArvo)
				{
					syontiSarjanArvo = lyonninArvo;
					syontiSarja = siirto;
				}
			}
			if (syontiSarjanArvo <= 0 
				|| syontiSarjanArvo > beta)
			{
				return evaluate(asema);
			}
			syvyys = 1;
			siirrot.clear();
			siirrot.push_back(syontiSarja);
		}
		//return evaluate(asema);
	}

	if (siirrot.size() == 0) // peli ohi
	{
		if (asema.getSiirtovuoro() == Asema::VALK)
			return VALK_HAVIO;
		else
			return MUST_HAVIO;
	}

	for (auto&& siirto : siirrot)
	{

		Asema testiasema = asema;
		int vari = testiasema.getSiirtovuoro();
		testiasema.paivitaAsema(&siirto);

		// t�m� on korotusta varten : jos siirrettiin juuri sotilasta niin nyt on daami
		if (asema.palautaRuutuNappula(siirto.getAlkuruutu()) == asema.VS
			&& siirto.getLoppuruutu().rivi == 7)
		{
			testiasema.lauta[siirto.getLoppuruutu().rivi][siirto.getLoppuruutu().sar] = asema.VD;
		}
		testiasema.setSiirtovuoro(Asema::MUST);
		double score = musta(syvyys - 1, testiasema, alpha, beta);
		//std::cout << "\nSyvyys: " << depth << " siirto: " << (char)(siirto.getAlkuruutu().sar + 'A') << siirto.getAlkuruutu().rivi + 1 << "-" << (char)(siirto.getLoppuruutu().sar + 'A') << siirto.getLoppuruutu().rivi + 1 << " score: " << score;
		if (score >= beta)
		{
			//	std::cout << "\nBeta katkaisu";
			return beta;
		}
		if (score > alpha)
			alpha = score;

	}
	return alpha;
}


double Minimax::musta(const int depth, Asema asema, double alpha, double beta)
{
	int syvyys = depth;
	std::vector<Siirto>siirrot;
	asema.annaLaillisetSiirrot(siirrot);


	if (siirrot.size() == 0) // peli ohi
	{
		if (asema.getSiirtovuoro() == Asema::VALK)
			return VALK_HAVIO;
		else
			return MUST_HAVIO;
	}


	if (depth <= 0)
	{
		double aseman_arvo_erotus = evaluate(asema);;
		aseman_arvo_erotus = beta - evaluate(asema);
		std::vector<Siirto> syovat_siirrot;
		std::copy_if(siirrot.begin(), siirrot.end(), std::back_inserter(syovat_siirrot), [](Siirto& siirto) {
			return siirto.syo;
		});

		if(syovat_siirrot.size() == 0)
			return evaluate(asema);
		else
		{
			Siirto syontiSarja;

			double syontiSarjanArvo = 0;
			for (auto&& siirto : syovat_siirrot)
			{
				double lyonninArvo = -syontiSarjaEvaluointi(siirto, asema, Asema::MUST);
				if (lyonninArvo < syontiSarjanArvo)
				{
					syontiSarjanArvo = lyonninArvo;
					syontiSarja = siirto;
				}
			}
			if (syontiSarjanArvo >= 0
				|| syontiSarjanArvo <= alpha)
			{
				return evaluate(asema);
			}
			syvyys = 1;
			siirrot.clear();
			siirrot.push_back(syontiSarja);
		}

		//return evaluate(asema);
	}


	for (auto&& siirto : siirrot)
	{
		Asema testiasema = asema;

		// T�m� on korotusta varten
		// huomaa ett� verrataan ensin alkuper�iseen lautaan ja sitten vasta testiasemassa p�ivitet��n nappula
		if (asema.palautaRuutuNappula(siirto.getAlkuruutu()) == asema.MS
			&& siirto.getLoppuruutu().rivi == 0)
		{
			testiasema.lauta[siirto.getLoppuruutu().rivi][siirto.getLoppuruutu().sar] = asema.MD;
		}

		testiasema.paivitaAsema(&siirto);
		testiasema.setSiirtovuoro(Asema::VALK);

		double score = valkoinen(syvyys - 1, testiasema, alpha, beta);
		//std::cout << "\nSyvyys: " << depth << " siirto: " << (char)(siirto.getAlkuruutu().sar + 'A') << siirto.getAlkuruutu().rivi + 1 << "-" << (char)(siirto.getLoppuruutu().sar + 'A') << siirto.getLoppuruutu().rivi + 1 << " score: " << score;
		if (score <= alpha)
		{
			//std::cout << "\nAlpha katkaisu";
			return alpha;
		}
		if (score < beta)
		{
			beta = score;
		}

	}
	//std::cout << ".";
	return beta;
}


void Minimax::jarjestaSiirrot(std::vector<Siirto>& siirrot, Asema asema, int vari, double arvo, double alpha, double beta)
{
	std::vector<siirtoArvo> jarjestetty;
	for (auto&& siirto : siirrot)
	{
		Asema testiasema = asema;
		testiasema.paivitaAsema(&siirto);


		if (vari == Asema::VALK)
		{

			testiasema.setSiirtovuoro(Asema::MUST);
			double score = musta(0, testiasema, alpha, beta);
			jarjestetty.push_back({ score, siirto });


		}
		else
		{
			testiasema.setSiirtovuoro(Asema::VALK);
			double score = valkoinen(0, testiasema, alpha, beta);
			jarjestetty.push_back({ score, siirto });
		}

	}
	if (vari == Asema::VALK)
		std::sort(jarjestetty.begin(), jarjestetty.end(), [](siirtoArvo& oik, siirtoArvo& vas) {
		return oik.arvo > vas.arvo;
	});
	else
		std::sort(jarjestetty.begin(), jarjestetty.end(), [](siirtoArvo& oik, siirtoArvo& vas) {
		return oik.arvo < vas.arvo;
	});

	siirrot.clear();
	for (auto&& sa : jarjestetty)
	{
		siirrot.push_back(sa.siirto);
	}
}


int laskeNappulat(Asema& asema)
{
	int maara = 0;
	for (int i = 0; i < 8; i++)
	{
		for (int k = 0; k < 8; k++)
		{
			if (asema.lauta[i][k] != asema.tyhja)
				maara++;
		}
	}
	return maara;
}

siirtoArvo Minimax::arvaus(const int depth, Asema asema, double alpha, double beta)
{
	int laskusyvyys = depth;
	int keskipeli = depth + 1;
	int loppupeli = depth + 2;



	if (laskeNappulat(asema) < 10)
		laskusyvyys = keskipeli;
	if (laskeNappulat(asema) < 7)
		laskusyvyys = loppupeli;

	//parasSiirto = Siirto(Ruutu(0, 0), Ruutu(0, 0));
	std::vector<Siirto> parasSiirto;
	parasArvaus = Siirto(Ruutu(0, 0), Ruutu(0, 0));

	std::vector<Siirto>siirrot;
	asema.annaLaillisetSiirrot(siirrot);

	double arvo;
	if (asema.getSiirtovuoro() == Asema::VALK) arvo = VALK_HAVIO;
	else
	{
		arvo = MUST_HAVIO;
		// K��nnet�n mustan pelaajan siirrot, jotta ne k�yd��n "samassa" j�rjestyksess� kuin valkoisen
		std::reverse(siirrot.begin(), siirrot.end());
	}

	//yrit� j�rjest�� parhaiten
	//std::random_shuffle(siirrot.begin(), siirrot.end());

	jarjestaSiirrot(siirrot, asema, asema.getSiirtovuoro(), arvo, alpha, beta);

	for (auto&& siirto : siirrot)
	{

		Asema testiasema = asema;
		testiasema.paivitaAsema(&siirto);
		std::cout << "\n Pohdin " << asema.getSiirtovuoro() << " siirto: " << (char)(siirto.getAlkuruutu().sar + 'A') << siirto.getAlkuruutu().rivi + 1 << "-" << (char)(siirto.getLoppuruutu().sar + 'A') << siirto.getLoppuruutu().rivi + 1;

		if (asema.getSiirtovuoro() == Asema::VALK)
		{
			testiasema.setSiirtovuoro(Asema::MUST);
			double score = musta(laskusyvyys, testiasema, alpha, beta);

			std::cout << " Sen score: " << score;
			if (score == arvo)
				parasSiirto.push_back(siirto);
			if (score > arvo)
			{
				arvo = score;
				parasSiirto.clear();
				parasSiirto.push_back(siirto);
			}
			if (score > alpha)
				alpha = score;
		}
		else
		{
			testiasema.setSiirtovuoro(Asema::VALK);
			double score = valkoinen(laskusyvyys, testiasema, alpha, beta);
			std::cout << " Sen score: " << score;

			if (score == arvo)
				parasSiirto.push_back(siirto);
			if (score < arvo)
			{
				arvo = score;
				parasSiirto.clear();
				parasSiirto.push_back(siirto);
			}
			if (score < beta)
				beta = score;
		}

		// jos parhaita siirtoja useampia, arvo yks

	}

	if (parasSiirto.size() != 0)
	{

		std::random_shuffle(parasSiirto.begin(), parasSiirto.end());
		Siirto palautaSiirto = parasSiirto[0];
		return{ arvo, palautaSiirto };
	}
	else
	{
		return{ arvo, Siirto(0,0) }; // peli ohi
	}



}

struct tulevaisuusArvo
{
public:
	std::future<double> arvo;
	Siirto siirto;
	bool operator==(const tulevaisuusArvo& rhs)
	{
		return siirto == rhs.siirto;
	}
};

siirtoArvo Minimax::parallel_arvaus(const int depth, Asema asema, double alpha, double beta)
{
	
	int laskusyvyys = depth;
	int keskipeli = depth + 1;
	int loppupeli = depth + 2;
	//parasSiirto = Siirto(Ruutu(0, 0), Ruutu(0, 0));
	std::vector<siirtoArvo> parasSiirto;
	parasArvaus = Siirto(Ruutu(0, 0), Ruutu(0, 0));

	std::vector<tulevaisuusArvo> arvot;
	std::vector<Siirto>siirrot;
	asema.annaLaillisetSiirrot(siirrot);

	double arvo;

	if (asema.getSiirtovuoro() == Asema::VALK) arvo = VALK_HAVIO;
	else
	{
		arvo = MUST_HAVIO;
		// K��nnet�n mustan pelaajan siirrot, jotta ne k�yd��n "samassa" j�rjestyksess� kuin valkoisen
		std::reverse(siirrot.begin(), siirrot.end());
	}

	//yrit� j�rjest�� parhaiten
	//std::random_shuffle(siirrot.begin(), siirrot.end());

	jarjestaSiirrot(siirrot, asema, asema.getSiirtovuoro(), arvo, alpha, beta);

	if (laskeNappulat(asema) < 12)
		laskusyvyys = keskipeli;
	if (laskeNappulat(asema) < 8)
		laskusyvyys = loppupeli;

	for (auto&& siirto : siirrot)
	{
	
		Asema testiasema = asema;
		testiasema.paivitaAsema(&siirto);


		if (asema.getSiirtovuoro() == Asema::VALK)
		{

			testiasema.setSiirtovuoro(Asema::MUST);
			// laitetaan threadi sis��n jos s�ikeit� riitt��
			if (arvot.size() < std::thread::hardware_concurrency())
//if (arvot.size()  < 1)

				arvot.push_back({ std::async(&Minimax::musta, *this, depth, testiasema, alpha, beta), siirto });
			else
			{
				// odotetaan ensimmm�isen s�ikeen tulosta
				bool odota = true;
				while (odota)
				{
					for (auto&& thread_arvo : arvot)
					{
						if (thread_arvo.arvo.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
						{
							double score = thread_arvo.arvo.get();

							std::cout << "\n Pohdin " << asema.getSiirtovuoro() << " siirto: " << (char)((thread_arvo).siirto.getAlkuruutu().sar + 'A') << (thread_arvo).siirto.getAlkuruutu().rivi + 1 << "-" << (char)((thread_arvo).siirto.getLoppuruutu().sar + 'A') << (thread_arvo).siirto.getLoppuruutu().rivi + 1 << " score: " << score;
							
							if (score == arvo)
								parasSiirto.push_back({ score, (thread_arvo).siirto });
							if (score > arvo)
							{
								arvo = score;
								parasSiirto.clear();
								parasSiirto.push_back({ score, (thread_arvo).siirto });;
							}
							//if (score > alpha)
								//alpha = score;
							odota = false;
							arvot.erase(std::remove(arvot.begin(), arvot.end(), thread_arvo), arvot.end());
							arvot.push_back({ std::async(&Minimax::musta, *this, depth, testiasema, alpha, beta), siirto });
							break;
						}
					}
				}
			}
		}
		else if (asema.getSiirtovuoro() == Asema::MUST)
		{
			testiasema.setSiirtovuoro(Asema::VALK);
			if (arvot.size() < std::thread::hardware_concurrency())
				arvot.push_back({ std::async(&Minimax::valkoinen, *this, depth, testiasema, alpha, beta), siirto });
			else
			{
				bool odota = true;
				while (odota)
				{
					for (auto&& thread_arvo : arvot)
					{
						if (thread_arvo.arvo.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
						{

							double score = thread_arvo.arvo.get();

							std::cout << "\n Pohdin " << asema.getSiirtovuoro() << " siirto: " << (char)((thread_arvo).siirto.getAlkuruutu().sar + 'A') << (thread_arvo).siirto.getAlkuruutu().rivi + 1 << "-" << (char)((thread_arvo).siirto.getLoppuruutu().sar + 'A') << (thread_arvo).siirto.getLoppuruutu().rivi + 1 << " score: " << score;
							

							if (score == arvo)
								parasSiirto.push_back({ score, (thread_arvo).siirto });
							if (score < arvo)
							{
								arvo = score;
								parasSiirto.clear();
								parasSiirto.push_back({ score, (thread_arvo).siirto });
							}
							//if (score < beta)
							//	beta = score;
							odota = false;
							arvot.erase(std::remove(arvot.begin(), arvot.end(), thread_arvo), arvot.end());
							arvot.push_back({ std::async(&Minimax::valkoinen, *this, depth, testiasema, alpha, beta), siirto });
							break;
						}
					}
				}
			}
		}
	}

	// varmistetaan ett� kaikki threadit on p��ttynyt
	for (auto&& thread_arvo = arvot.begin(); thread_arvo != arvot.end(); thread_arvo++)
	{
		double score = (*thread_arvo).arvo.get();
		std::cout << "\n Pohdin " << asema.getSiirtovuoro() << " siirto: " << (char)((*thread_arvo).siirto.getAlkuruutu().sar + 'A') << (*thread_arvo).siirto.getAlkuruutu().rivi + 1 << "-" << (char)((*thread_arvo).siirto.getLoppuruutu().sar + 'A') << (*thread_arvo).siirto.getLoppuruutu().rivi + 1 << " score: " << score;
		if (score == arvo)
			parasSiirto.push_back({ score, (*thread_arvo).siirto });
		if (asema.getSiirtovuoro() == Asema::VALK)
		{
			if (score > arvo)
			{
				arvo = score;
				parasSiirto.clear();
				parasSiirto.push_back({ score, (*thread_arvo).siirto });
			}
			/*if (score > alpha)
				alpha = score;*/

		}
		else if (asema.getSiirtovuoro() == Asema::MUST)
		{

			if (score < arvo)
			{
				arvo = score;
				parasSiirto.clear();
				parasSiirto.push_back({ score, (*thread_arvo).siirto });
			}
			//if (score < beta)
			//	beta = score;
		}
	}


		// jos parhaita siirtoja useampia, arvo yks
		if (parasSiirto.size() != 0)
		{

			std::random_shuffle(parasSiirto.begin(), parasSiirto.end());
			
				return parasSiirto[0];
		}
		else
		{
			return{ arvo, Siirto(0,0) }; // peli ohi
		}



}


double Minimax::Qhaku(int depth, Asema& asema, double alpha, double beta)
{
	// koodia CPW-moottorista, 
	// https://github.com/nescitus/cpw-engine

	/* get a "stand pat" score */
	double val = evaluate(asema);
	if (depth == 0)
		return val;
	double stand_pat = val;

	/* check if stand-pat score causes a beta cutoff */
	if (val >= beta)
		return beta;

	/* check if stand-pat score may become a new alpha */
	if (alpha < val)
		alpha = val;

	std::vector<Siirto>siirrot;
	asema.annaLaillisetSiirrot(siirrot);

	for (auto&& siirto : siirrot)
	{

		Asema testiasema = asema;
		testiasema.paivitaAsema(&siirto);
		if (testiasema.getSiirtovuoro() == Asema::VALK)
		{
			testiasema.setSiirtovuoro(Asema::MUST);
		}
		else
			testiasema.setSiirtovuoro(Asema::VALK);

		///**********************************************************************
		//*  Delta cutoff - a move guarentees the score well below alpha, so    *
		//*  there's no point in searching it. We don't use his heuristic in    *
		//*  the endgame, because of the insufficient material issues.          *
		//**********************************************************************/

		//if ((stand_pat + e.PIECE_VALUE[movelist[i].piece_cap] + 200 < alpha)
		//	&& (b.piece_material[!b.stm] - e.PIECE_VALUE[movelist[i].piece_cap] > e.ENDGAME_MAT)
		//	&& (!move_isprom(movelist[i])))
		//	continue;

		val = -Qhaku(depth - 1, testiasema, -beta, -alpha);

		if (val > alpha) {
			if (val >= beta) return beta;
			alpha = val;
		}
	}
	return alpha;
}

//
//quis(board, alpha, beta)
//http://mediocrechess.blogspot.fi/2006/12/guide-quiescent-search-and-horizon.html
//eval = evaluatePosition();
//if (value >= beta)
//return beta;
//
//if (value > alpha)
//alpha = value;
//
//generateCaptures();
//forEveryCapture
//{
//	makeMove(nextCapture);
//eval = -quis(tempBoard, -beta,-alpha);
//unmakeMove();
//
//if (eval >= beta)
//return beta;
//
//if (value > alpha)
//alpha = eval;
//}
//
//return alpha;

double Minimax::syontiSarjaEvaluointi(Siirto siirto, Asema asema, int vari)
{
	double score = 0;

		Asema testiasema = asema;
		testiasema.paivitaAsema(&siirto);
		if (vari == Asema::VALK)
			testiasema.setSiirtovuoro(Asema::MUST);
		else
			testiasema.setSiirtovuoro(Asema::VALK);
		std::vector<Siirto> siirrot;
		testiasema.annaLaillisetSiirrot(siirrot);

		Siirto uhkaavinSiirto = Siirto(0, 0);
		double uhkaajanArvo = KUNINGAS_ARVO;
		for (auto&& vastsiirto : siirrot)
		{
			if (vastsiirto.syo && vastsiirto.getLoppuruutu() == siirto.getLoppuruutu())
			{
				double arvo = 0;
				if (testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.VS
					|| testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.MS)
				{
					arvo = SOTILAS_ARVO;
				}
				else if (testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.ML
					|| testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.VL)
				{
					arvo = LAHETTI_ARVO;
				}
				else if (testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.MR
					|| testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.VR)
				{
					arvo = RATSU_ARVO;
				}
				else if (testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.MT
					|| testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.VT)
				{
					arvo = TORNI_ARVO;
				}
				else if (testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.MD
					|| testiasema.palautaRuutuNappula(vastsiirto.getAlkuruutu()) == testiasema.VD)
				{
					arvo = DAAMI_ARVO;
				}
				if (arvo < uhkaajanArvo)
				{
					uhkaavinSiirto = vastsiirto;
					uhkaajanArvo = arvo;
				}
			}
		}
		// etsi kaikki hy�kk��j�t ja nouki pienin
	/* skip if the square isn't attacked anymore by this side */
	double value = 0;
	if (!(uhkaavinSiirto == Siirto(0,0)))
	{
		/* Do not consider captures if they lose material, therefor max zero */
		if(vari == Asema::VALK)
			value = uhkaajanArvo - syontiSarjaEvaluointi(uhkaavinSiirto, testiasema, Asema::MUST);
		else
			value = uhkaajanArvo - syontiSarjaEvaluointi(uhkaavinSiirto, testiasema, Asema::VALK);
	}

	if (value <= 0)
		return 0;
	return value;
}